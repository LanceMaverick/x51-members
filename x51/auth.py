import functools
import jwt
from jwt import ExpiredSignatureError
from datetime import datetime
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, jsonify
)
from werkzeug.security import check_password_hash, generate_password_hash
from flask_security import login_user, logout_user, current_user, login_required
from flask_security.forms import ChangePasswordForm
from flask import current_app as app, make_response

from x51.models import User
from x51.forms import LoginForm
from .database import db

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/register')
def register():
    return "Registering is handled manually by the admin."

#login view
@bp.route('/login', methods=('GET', 'POST'))
def login():
    next_param = request.args.get('next')
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm(request.form)
    if form.validate_on_submit():
        username = form.username.data.lower()
        password = form.password.data
        remember = form.remember.data
        error = None
        user = User.query.filter_by(username=username).first()
        if user is None or not user.check_password(password):
            flash('Incorrect username or password.','danger')
            return redirect(url_for('auth.login', form=form, next=next_param))
        if user.active:
            login_user(user, remember=remember)
            user.last_login = datetime.now()
            db.session.commit()
        else:
            flash('This account has been suspended. Contact Wazzerbosh.','danger')
        #check if user has logged in before and changed temporary password.
        if not user.confirmed_at:
            flash('You have not changed your password from the temporary one. Please do so now.', 'warning')
            return redirect(url_for('auth.account'))
        elif next_param:
                return redirect(next_param)
        #    session.clear()
        #    session['user_id'] = user.id
        return redirect(url_for('index', error=error))

    return render_template('auth/login.html', form=form)

@login_required
@bp.route('/account', methods=('GET', 'POST'))
def account():
    user = current_user
    form = ChangePasswordForm(request.form)
    if request.method =='POST':
        if form.validate():
            password = form.new_password.data
            user.set_password(password)
            user.confirmed_at = datetime.now()
            db.session.commit()
            flash('Password updated!', 'success')
        else:
            flash('ERROR', 'danger')
            for e in form.password.errors:
                flash(e, 'danger')
    return render_template('auth/account.html', form=form)

@bp.before_app_request
def load_logged_in_user():
    g.user = current_user

@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

## TODO: combine all these decorators into a single role required function
#officer auth decorator
def officer_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if not current_user.has_role('officer'):
            return(redirect(url_for('auth.denied')))
        return view(**kwargs)
    return wrapped_view

#curator auth decorator
def curator_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if not current_user.has_role('officer') and not current_user.has_role('curator'):
            return(redirect(url_for('auth.denied')))
        return view(**kwargs)
    return wrapped_view

#instructor auth decorator
def instructor_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if not current_user.has_role('officer') and not current_user.has_role('instructor'):
            return(redirect(url_for('auth.denied')))
        return view(**kwargs)
    return wrapped_view

def admin_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if not current_user.has_role('admin'):
            return(redirect(url_for('auth.denied')))
        return view(**kwargs)
    return wrapped_view


@bp.route('/denied')
def denied():
    return render_template('auth/denied.html')

def token_required(f):
    # wrapper for token protected api endpoints
    @functools.wraps(f)
    def decorator(*args, **kwargs):

        token = None

        if 'x-access-tokens' in request.headers:
            token = request.headers['x-access-tokens']

        if not token:
            return make_response(jsonify({'message': 'a valid token is missing'}), 400)

        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
            app.logger.info("TOKEN VALIDATOR %s", data['username'])
            current_user = User.query.filter_by(username=data['username']).first()
        except ExpiredSignatureError:
            return make_response(jsonify({'message': 'token expired'}), 401)
        except Exception as e:
            app.logger.info(e)
            return make_response(jsonify({'message': 'token is invalid'}), 401)

        return f(current_user, *args, **kwargs)
    return decorator