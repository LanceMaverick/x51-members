import functools
from requests import Session, Request 
import patreon
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from flask_security import current_user

from flask import current_app as app

from x51.auth import officer_required, admin_required
from .database import db
from .config import PatreonConfig as pc

bp = Blueprint('patreon', __name__, url_prefix='/patreon')

@bp.route('/')
@admin_required
def login():
    s = Session()
    
    params = dict(
        client_id = pc.client_id,
        response_type='code',
        redirect_uri=pc.redirect_uri,)
    
    
    p = Request('GET', 'https://www.patreon.com/oauth2/authorize', params=params).prepare()
    app.logger.info(p.url)
    return render_template('patreon/login.html', oauth_url=p.url)
    


@bp.route('/oauth/redirect')
def oauth_redirect():
    app.logger.info(request.args)
    oauth_client = patreon.OAuth(pc.client_id, pc.client_secret)
    tokens = oauth_client.get_tokens(request.args.get('code'), '/oauth/redirect')
    app.logger.info(tokens)
    access_token = tokens['access_token']

    api_client = patreon.API(access_token)
    user_response = api_client.get_identity()
    user = user_response.data()
    memberships = user.relationship('memberships')
    membership = memberships[0] if memberships and len(memberships) > 0 else None

    return render_template('patreon/index.html', memberships=memberships)


