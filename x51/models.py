from datetime import datetime
from .database import db
#from werkzeug.security import check_password_hash, generate_password_hash
from flask_security.utils import hash_password, verify_and_update_password
from flask_security import UserMixin, RoleMixin, utils


roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))

#logging officer for trainings
training_users = db.Table('trainings_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('training_id', db.Integer(), db.ForeignKey('training.id')))

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False, index=True)
    password = db.Column(db.String(), nullable=False)
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    last_login = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))
    
    #one to many rel. with training logs
    #trainings = db.relationship('Training', secondary=training_users,
    #                        backref=db.backref('users', lazy='dynamic'))
    
    #one to one rel. with Member file
    member = db.relationship("Member", uselist=False, backref="user")

    missions = db.relationship("Mission", back_populates="user")
    archives = db.relationship("Archive", back_populates="user")

    def check_password(self, password):
        return verify_and_update_password(password, self)
    
    def set_password(self, password):
        self.password = hash_password(password)

    def __repr__(self):
        return '<User %r>' % self.username
    

class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))
    

#reference table for relationships
###DEPRACATED
training_register = db.Table('training_register',
        db.Column('member_id', db.Integer, db.ForeignKey('member.id')),
        db.Column('training_id', db.Integer, db.ForeignKey('training.id')),
        )
####
attendee_register = db.Table('attendee_register',
        db.Column('member_id', db.Integer, db.ForeignKey('member.id')),
        db.Column('training_id', db.Integer, db.ForeignKey('training.id')),
        )
advisors_register = db.Table('advisors_register',
        db.Column('member_id', db.Integer, db.ForeignKey('member.id')),
        db.Column('training_id', db.Integer, db.ForeignKey('training.id')),
        )
nonadvisors_register = db.Table('nonadvisors_register',
        db.Column('member_id', db.Integer, db.ForeignKey('member.id')),
        db.Column('training_id', db.Integer, db.ForeignKey('training.id')),
        )

award_register = db.Table('award_register',
        db.Column('member_id', db.Integer, db.ForeignKey('member.id')),
        db.Column('award_id', db.Integer, db.ForeignKey('award.id')),
        )

class SpecTraining(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True, nullable=False)

    def members(self):
        return Member.query.filter_by(spec_training_id = self.id, active=True).all()


class Member(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), unique=True, nullable=False)
    join_date = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)
    location = db.Column(db.String(120), nullable=True)
    geozone = db.Column(db.String(120), nullable=True) #zone deciding what trainings they will join
    notes = db.Column(db.Text(),  nullable=True)
    pilot_rank = db.Column(db.String(120),  nullable=False)
    wt = db.Column(db.Boolean(),  nullable=False)
    il2 = db.Column(db.Boolean(),  nullable=False)
    dcs = db.Column(db.Boolean(),  nullable=False)
    arma = db.Column(db.Boolean(),  nullable=False)
    bms =  db.Column(db.Boolean(),  nullable=False, default=False)
    req_training = db.Column(db.String(120),  nullable=False)
    recent_attended = db.Column(db.Integer(),  default=0) #recent activity on attended trainings
    absent_trainings = db.Column(db.Integer(),  default=0) #recent activity on unadvised misses
    warn = db.Column(db.Boolean(),  default=False)
    total_warns = db.Column(db.Integer(),  default=0)
    on_reserve = db.Column(db.Boolean(),  default=False)
    reserve_date = db.Column(db.DateTime())
    active = db.Column(db.Boolean(),  default=True)
    reserve_date = db.Column(db.DateTime())

    #specialisation class, e.g A2G etc

    #many-to-many relationships between members and trainings
    attended = db.relationship('Training', secondary = attendee_register, backref = db.backref('attendees'))
    advised = db.relationship('Training', secondary = advisors_register, backref = db.backref('advisories'))
    missing = db.relationship('Training', secondary = nonadvisors_register, backref = db.backref('nonadvisories'))

    #one to one relationship with user account
    account_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    #one to one specialisation training
    spec_training_id = db.Column(db.Integer, db.ForeignKey('spec_training.id'))

    #relationship for awards
    awards = db.relationship('Award', secondary = award_register, backref = db.backref('awards'))

    def spec(self):
        if self.spec_training_id:
            sp = SpecTraining.query.filter_by(id = self.spec_training_id).first()
            if sp:
                return sp.name
        return "None"

    def __repr__(self):
        return '<Member %r>' % self.username

class Training(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sim = db.Column(db.String(120), nullable=False)
    date = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)
    geozone = db.Column(db.String(120), nullable=True)
    description = db.Column(db.Text(), nullable=False)

    officers = db.relationship('User', secondary = training_users, backref = db.backref('officers'))

    #attendees = db.Column(db.String(1000), nullable=False)
    def __repr__(self):
        return '<Training %r %r>' % (self.sim, self.date)

class Award(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), nullable=False)
    category = db.Column(db.String(120), nullable=False)
    tier = db.Column(db.Integer, nullable=True, default=1)
    icon_url = db.Column(db.String(500), nullable=True)
    description = db.Column(db.Text(), nullable=False)
    prize = db.Column(db.Text(), nullable=False)

    #many to many rel. for awards - members


class Feed(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), nullable=False)
    date = db.Column(db.DateTime(), default=datetime.utcnow)
    xml = db.Column(db.Text())

########################
# Virtual Sorties Models
########################
class SortiesBase:
    def get_side(self):
        al = 'UNDEFINED'
        if self.platform =='dcs':
            if self.nationality =='blue':
                al = 'BLUEFOR'
            elif self.nationality == 'red':
                al = 'REDFOR'
        elif self.platform == 'il2' or self.platform == 'wt':
            if self.nationality == 'blue':
                al = 'Axis'
            elif self.nationality == 'red':
                al = "Allies"
        #ARMA?
        return al

class Sortie(db.Model, SortiesBase):
    id = db.Column(db.Integer, primary_key=True)
    platform = db.Column(db.String(120), nullable=False)
    date = db.Column(db.DateTime(), default=datetime.utcnow)
    nationality = db.Column(db.String(120), nullable=True)
    description = db.Column(db.Text(), nullable=False)
    lead_id = db.Column(db.Integer, nullable=False)
    lead_pilot_id = db.Column(db.Integer, nullable=True)
    lead_pilot_name = db.Column(db.String(120), nullable=True) 

class Flight(db.Model, SortiesBase):
    id = db.Column(db.Integer, primary_key=True)
    sortie_id = db.Column(db.Integer, nullable=False)
    user_id = db.Column(db.Integer, nullable=False)
    pilot_id = db.Column(db.Integer, nullable=False)
    pilot_name = db.Column(db.String(120), nullable=True)
    air_kills = db.Column(db.Integer, nullable=False, default=0)
    assists = db.Column(db.Integer, nullable=False, default=0)
    ground_kills = db.Column(db.Integer, nullable=False, default=0)
    successes = db.Column(db.Integer, nullable=False, default=0)
    failures = db.Column(db.Integer, nullable=False, default=0)
    aircraft_losses = db.Column(db.Integer, nullable=False, default=0)
    xp = db.Column(db.Integer, nullable=True, default=0)
    died = db.Column(db.Boolean())

class Pilot(db.Model, SortiesBase):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String(120), nullable=False)
    nationality = db.Column(db.String(120), nullable=True)
    platform = db.Column(db.String(120), nullable=True)
    rank = db.Column(db.Integer, nullable=False, default=1)
    air_kills = db.Column(db.Integer, default=0)
    assists = db.Column(db.Integer, default=0)
    ground_kills = db.Column(db.Integer, default=0)
    successes = db.Column(db.Integer, default=0)
    failures = db.Column(db.Integer, default=0)
    aircraft_losses = db.Column(db.Integer, default=0)
    xp = db.Column(db.Integer, default=0)
    alive = db.Column(db.Boolean())

    

        
##########################
# Uploaded Mission Model #
##########################

class Mission(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text(), nullable=False)
    description = db.Column(db.Text(), nullable=True)
    category = db.Column(db.String(120), nullable=False)
    briefing = db.Column(db.Text(), nullable=True)
    mission = db.Column(db.Text(), nullable=False)
    date = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)

    #rel with uploader
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", back_populates="missions")

##########################
# Uploaded Archive Model #
##########################

class Archive(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text(), nullable=False)
    description = db.Column(db.Text(), nullable=True)
    file = db.Column(db.Text(), nullable=False)
    date = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)

    #rel with uploader
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", back_populates="archives")


   
class Kneeboard(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120), nullable=False)
    date = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)
    summary = db.Column(db.Text(), nullable=True)
    roe = db.Column(db.Text(), nullable=True)
    flights = db.relationship("KbFlight", back_populates="kb")
    entities = db.relationship("KbEntity", back_populates="kb")
    

class KbFlight(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), nullable=False)
    airframe = db.Column(db.String(120), nullable=False)
    task = db.Column(db.String(120), nullable=False)
    freq = db.Column(db.Numeric(), nullable=True)
    
    kb_id = db.Column(db.Integer, db.ForeignKey('kneeboard.id'))
    kb = db.relationship("Kneeboard", back_populates="flights")

class KbEntity(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), nullable=False)
    type = db.Column(db.String(120), nullable=False)
    freq = db.Column(db.Numeric(), default=0)

    rwy = db.Column(db.String(5), nullable=True)
    ils = db.Column(db.Numeric(), default=0)
    tacan = db.Column(db.String(5), nullable=True)
    alt = db.Column(db.Integer, nullable=True)

    kb_id = db.Column(db.Integer, db.ForeignKey('kneeboard.id'))
    kb = db.relationship("Kneeboard", back_populates="entities")

class ModSyncVersion(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    major = db.Column(db.Integer, nullable=False)
    minor = db.Column(db.Integer, nullable=False)
    patch = db.Column(db.Integer, nullable=False)

    def acceptable(self, version):
        if version == [0,0,0]:
            return True
            
        this_ver = [self.major, self.minor, self.patch]

        for i, v in enumerate(this_ver):
            if version[i] > v:
                return True
            elif version[i] < v:
                return False
        return True #if here, versions are identical
