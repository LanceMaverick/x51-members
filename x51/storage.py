from datetime import datetime 
import boto3
from boto3 import session
from botocore.client import Config
from boto3.s3.transfer import S3Transfer
from x51.config import S3Config
from flask import current_app as app




SESSION = session.Session()
CLIENT = SESSION.client('s3',
                        region_name = S3Config.REGION, 
                        endpoint_url=S3Config.ENDPOINT,
                        aws_access_key_id=S3Config.ACCESS_ID,
                        aws_secret_access_key=S3Config.SECRET_KEY)

DCS_MODS = 'x51Sync/dcs/mods/'
DCS_LIVERIES = 'x51Sync/dcs/liveries/'

def get_file_list(bucket_name):
    #will only return first 1000 results
    #bucket = CLIENT.get_bucket('bucket_name')
    return [key['Key'] for key in CLIENT.list_objects(Bucket=bucket_name)['Contents']]

def get_detailed_file_list(bucket_name, prefix=''):
    data = []
    #cont_token = ''

    paginator = CLIENT.get_paginator('list_objects_v2')
    pages = paginator.paginate(Bucket=bucket_name, Prefix=prefix)

    for page in pages:
        for object in page['Contents']:
            data.append(object)

        #resp = CLIENT.list_objects_v2(Bucket=bucket_name, Prefix=prefix, ContinuationToken=cont_token)
        #contents = resp['Contents']
        #data = data + contents
        #if not resp['IsTruncated']:
        #   break
        #cont_token = resp['ContinuationToken']

    return [dict(
        key=key['Key'], 
        date= key['LastModified'].strftime('%Y-%m-%d %H:%M:%S'),
        size=key['Size']) for key in data]

def parse_date(date_str):
    date_fmt = '%a, %d %b %Y %H:%M:%S %Z'
    dt = datetime.strptime(date_str, date_fmt)
    return dt.srftime('%Y-%m-%d %H:%M:%S')

def get_presigned_upload_url(object_name, bucket_name, expiry):
    params = {
        'Bucket': bucket_name,
        'Key': object_name,
        'ACL': 'public-read'
    }

    response = CLIENT.generate_presigned_url('put_object',
                                                    Params= params,
                                                    ExpiresIn=expiry)
    return response

def delete_object(object_name, bucket_name):
    params = {
        'Bucket': bucket_name,
        'Key': object_name
    }
    response = CLIENT.delete_object(Bucket=bucket_name, Key=object_name)
    
    return response

def delete_folder(bucket, prefix):
    
    responses = []
    for i in range(50):
        response = CLIENT.list_objects_v2(Bucket=bucket, Prefix=prefix)
        if not 'Contents' in response:
            break
        
        to_delete = [dict(Key= object['Key']) for object in response['Contents']]
        data = dict(
            Objects = to_delete,
            Quiet = True
        )
        resp = CLIENT.delete_objects(Bucket=bucket, Delete=data)
        app.logger.info(resp)
        responses.append(resp)
    return responses



def get_folders(bucket, prefix):
    response = CLIENT.list_objects_v2(Bucket=bucket, Prefix = prefix, Delimiter = '/')
    app.logger.info(response)
    if not 'CommonPrefixes' in response:
        return []
    return [prefix['Prefix'][:-1].split('/')[-1] for prefix in response['CommonPrefixes']]
    
#DCS specific endpoint handlers
def get_dcs_mods(mod_type):
    
    prefix=DCS_MODS+mod_type+'/'
    resp = get_folders('x51sqn', prefix)
    app.logger.info(resp)
    return resp

def get_dcs_mod(mod_type, mod):
    prefix = '{}{}/{}/'.format(DCS_MODS, mod_type, mod)
    return get_detailed_file_list('x51sqn', prefix)


def delete_dcs_mod(mod_type, mod):
    prefix='{}{}/{}/'.format(DCS_MODS, mod_type, mod)
    responses = delete_folder('x51sqn', prefix)
    return responses
 
def delete_dcs_livery(aircraft, livery):
    prefix='{}{}/{}/'.format(DCS_LIVERIES, aircraft, livery)
    responses = delete_folder('x51sqn', prefix)
    return responses

def delete_dcs_livery_aircraft(aircraft):
    prefix='{}{}/'.format(DCS_LIVERIES, aircraft)
    responses = delete_folder('x51sqn', prefix)
    return responses


def get_dcs_liveries(aircraft):
    
    prefix=DCS_LIVERIES+aircraft+'/'
    resp = get_folders('x51sqn', prefix)
    app.logger.info(resp)
    return resp

def get_dcs_livery_aircraft():
    
    resp = get_folders('x51sqn', DCS_LIVERIES)
    app.logger.info(resp)
    return resp

def get_dcs_livery(aircraft, livery):
    
    prefix = '{}{}/{}/'.format(DCS_LIVERIES, aircraft, livery)
    return get_detailed_file_list('x51sqn', prefix)