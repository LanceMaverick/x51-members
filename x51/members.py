import sys
import json
import datetime

#from bokeh.embed import components
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, send_from_directory
)
from flask import current_app as app
import flask_mobility
from werkzeug.exceptions import abort
try:
    from werkzeug import secure_filename
except ImportError:
    from werkzeug.utils import secure_filename

from flask_security import login_required, current_user

from wtforms import SelectField


from x51.auth import instructor_required, officer_required, admin_required
from x51.models import Member, SpecTraining, Training, User, Award

from x51.forms import (NewTrainingForm, NewMemberForm, FullEditMemberForm, 
    EditTrainingForm, LinkMemberForm, ChooseAwardForm, ActivitySearchForm, 
    AllActivityForm, NewSpecForm)

from x51 import plots

from x51.helpers import Platforms, PilotRoles, calc_member_activity, calc_all_activity
#calc_activity
from x51.handlers import get_discord_json

from .database import db

bp = Blueprint('members', __name__)

@bp.route('/')
@login_required
def index():
    members = Member.query.filter_by(active=True).all()
    return render_template('members/index.html', members=members)

@bp.route('/ex_members')
@login_required
def ex_members():
    members = Member.query.filter_by(active=False).all()
    return render_template('members/index.html', members=members)


@bp.route('/create', methods=('GET', 'POST'))
@login_required
@officer_required
def create():
    form = NewMemberForm(request.form)
    form.spec_training_id.choices = all_specs()
    if request.method == 'POST':
    #if form.validate_on_submit():
        username = form.username.data
        wt = form.wt.data
        il2 = form.il2.data
        dcs = form.dcs.data
        arma = form.arma.data
        bms = form.bms.data
        req_training = form.req_training.data
        spec_training_id = form.spec_training_id.data
        geozone = form.geozone.data
        join_date = form.join_date.data
        location = form.location.data
       
#TODO: Fix. No longer works when there are multiple training types for each platform (e.g combat vs aerobatics)
#        if not getattr(form, str(form.req_training.data)).data:
#            flash("You must tick that the pilot has the correct platform if it's their primary!")
#        else:
        member = Member(
            username=username,
            pilot_rank='CADET',
            wt=wt,
            il2=il2,
            dcs=dcs,
            arma=arma,
            bms=bms,
            req_training=req_training,
            geozone = geozone,
            join_date = datetime.datetime.strptime(str(join_date), '%Y-%m-%d'),
            spec_training_id = spec_training_id,
            location = location)
            
        db.session.add(member)
        db.session.commit()
        app.logger.info('New user {} created successfully'.format(username))
        return redirect(url_for('members.index'))
        
    return render_template('members/create.html', form=form)


@bp.route('/pilot/<int:pilot_id>')
@login_required
def pilot(pilot_id):
    member = Member.query.filter_by(id=pilot_id).first()
    is_user = (current_user.member == member)
    is_officer = current_user.has_role('officer')

    if is_user or is_officer:
        training_counts = dict.fromkeys(Platforms.sim_keys)
        for k in training_counts.keys():
            training_counts[k] = dict()

        for sim in Platforms.sim_keys:
            for count in ['attended', 'advised', 'missing']:
                training_counts[sim][count] = len( [x for x in getattr(member, count) if getattr(x, "sim")==sim])

        script1, div1 = plots.member_training_graph(member)
        script2, div2 = plots.training_type_pie(member)

    if is_officer:
        return render_template(
            'members/pilot.html',
            member = member,
            trainings = training_counts,
            script1 = script1,
            div1 = div1,
            script2 = script2,
            div2 = div2,
        )
    elif not is_officer and is_user:
        return render_template(
            'members/public_pilot.html',
            member = member,
            trainings = training_counts,
            script1 = script1,
            div1 = div1,
            script2 = script2,
            div2 = div2,
            is_user = is_user,
            )
    elif not is_officer and not is_user:
        return render_template(
            'members/public_pilot.html',
            member = member,
            is_user = is_user,
            )
            

@bp.route('/testpilot/<int:pilot_id>')
@login_required
def testpilot(pilot_id):
    member = Member.query.filter_by(id=pilot_id).first()

def all_specs():
    specs = SpecTraining.query.all()
    return [(spec.id, spec.name) for spec in specs]

@bp.route('/pilot/<int:pilot_id>/edit', methods=('GET', 'POST'))
@login_required
@officer_required
def edit_pilot(pilot_id):
    member = Member.query.filter_by(id=pilot_id).first()
    form = FullEditMemberForm(request.form, obj=member)
    form.spec_training_id.choices = all_specs()
    if form.validate_on_submit():
        form.populate_obj(member)
        #if user account associated with member, suspend/reactivate account
        #depending on x51 membership
        if member.account_id:
            active = member.active
            user = User.query.filter_by(id=member.account_id).first()
            app.logger.info(user.id, user.active)
            user.active = member.active
        db.session.commit()
        app.logger.info('Pilot with id {} has been edited.'.format(pilot_id))
        return redirect(url_for('members.index'))
    return render_template('members/edit.html', form=form)


@bp.route('/delete_pilot/<int:pilot_id>', methods=['GET', 'POST'])
@login_required
@officer_required
def delete_pilot(pilot_id):
    member = Member.query.filter_by(id=pilot_id).first()
    if request.method == 'POST':
        if member.account_id:
            user = User.query.filter_by(id=member.account_id).first()
            app.logger.info(user.id, user.active)
            user.active = False
            user.member = None
        db.session.delete(member)
        db.session.commit()
        app.logger.info('Member with id {} has been deleted.'.format(pilot_id))
        return redirect(url_for('members.index'))

    return render_template('members/delete_pilot.html', member=member)



@bp.route('/all_activity', methods=['GET', 'POST'])
@login_required
@officer_required
def all_activity():
    now  = datetime.datetime.now()
    epoch = now - datetime.timedelta(days=90)
    members = Member.query.filter_by(active = True).all()
    result = calc_all_activity(members, epoch, now)
    form = AllActivityForm(request.form)
    if request.method == 'POST':
        if form.last_3_months.data:
            start_date = epoch
            end_date = now
        else:
            if not form.start_date.data:
                start_date = epoch
            else:
                start_date = datetime.datetime.strptime(str(form.start_date.data), '%Y-%m-%d')
            if not form.end_date.data:
                end_date = now
            else:
                end_date =  datetime.datetime.strptime(str(form.end_date.data), '%Y-%m-%d')
        result = calc_all_activity(members, start_date, end_date)
    
    app.logger.info(result)    
    return render_template('members/all_activity.html', form=form, result = result)

@bp.route('/activity/<int:pilot_id>', methods=['GET', 'POST'])
@login_required
@officer_required
def activity(pilot_id): 
    now  = datetime.datetime.now()
    epoch  =  datetime.datetime.strptime('2016-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
    member = Member.query.filter_by(id = pilot_id).first()
    result = calc_member_activity(
            member = member,
            start_date = epoch,
            end_date = now,
            geozone = member.geozone,
            sim = member.req_training)
    form = ActivitySearchForm(request.form)
    if request.method == 'POST':
        
        if form.required.data:
            geozone = member.geozone
            sim = member.req_training
        else:
            geozone = form.geozone.data
            sim = form.sim.data
        
        if form.all_dates.data:
            start_date = epoch
            end_date = now
        else:
            if not form.start_date.data:
                start_date = epoch
            else:
                start_date = datetime.datetime.strptime(str(form.start_date.data), '%Y-%m-%d')
            if not form.end_date.data:
                end_date = now
            else:
                end_date =  datetime.datetime.strptime(str(form.end_date.data), '%Y-%m-%d')

        app.logger.info(start_date)
        app.logger.info(end_date)

        result = calc_member_activity(
            member = member,
            start_date = start_date,
            end_date = end_date,
            geozone = geozone,
            sim = sim
        )
        #required_only = form.required_only.data

        #activity_data = calc_activity(
        #    start_date = datetime.datetime.strptime(str(start_date), '%Y-%m-%d'),
        #    end_date = datetime.datetime.strptime(str(end_date), '%Y-%m-%d'),
        #    geozone = geozone,
        #    sim = sim,
        #    required_only = required_only
        #)
    return render_template('members/activity.html', form=form, member=member, result=result)


@bp.route('/give_award/<int:pilot_id>', methods=['GET', 'POST'])
@login_required
@officer_required
def give_award(pilot_id):
    pilot = Member.query.filter_by(id=pilot_id).first()
    awards = Award.query.all()
    form = ChooseAwardForm(request.form)
    if request.method == 'POST':
        award_id = int(request.form['award'])
        award = Award.query.filter_by(id=award_id).first()
        if award in pilot.awards:
            flash('{} already has the "{}" award'.format(pilot.username, award.name), 'danger')
        else:
            pilot.awards.append(award)
            db.session.commit()
            flash('{} has been awarded: "{}"'.format(pilot.username, award.name), 'success')
        return redirect(url_for('members.pilot', pilot_id=pilot_id))

    return render_template('members/choose_award.html', member=pilot, awards=awards, form=form, v="Give")


@bp.route('/remove_award/<int:pilot_id>', methods=['GET', 'POST'])
@login_required
@officer_required
def remove_award(pilot_id):
    pilot = Member.query.filter_by(id=pilot_id).first()
    awards = pilot.awards
    form = ChooseAwardForm(request.form)
    if request.method == 'POST':
        award_id = int(request.form['award'])
        award = Award.query.filter_by(id=award_id).first()
        for i, a in enumerate(pilot.awards):
            if a == award:
                pilot.awards.pop(i)
            db.session.commit()
            flash('The "{}" award has been removed'.format(award.name), 'success')
            return redirect(url_for('members.pilot', pilot_id=pilot_id))
    return render_template('members/choose_award.html', member=pilot, awards=awards, form=form, v='Remove')    


@bp.route('/warn_pilot/<int:pilot_id>', methods=['GET', 'POST'])
@login_required
@officer_required
def warn_pilot(pilot_id):
    pilot = Member.query.filter_by(id=pilot_id).first()
    if request.method == 'POST':
        pilot.absent_trainings = 0
        pilot.warn = False
        pilot.total_warns += 1
        db.session.commit()
        return redirect(url_for('members.pilot', pilot_id=pilot_id))

    return render_template('members/warn_pilot.html', member=pilot)

@bp.route('/training/<int:training_id>')
@login_required
def training(training_id):
    training = Training.query.filter_by(id=training_id).first()
    return render_template('members/training.html', tr=training)


@bp.route('/add_training', methods=('GET', 'POST'))
@login_required
@instructor_required
def new_training():
    form = NewTrainingForm(request.form)
    if request.method == 'POST':
        dt = form.date.data
        sim = form.sim.data
        description = form.description.data
        pilots = form.attendees.data
        geozone = form.geozone.data
        training = Training(
            date = datetime.datetime.strptime(str(dt), '%Y-%m-%d'),
            sim = sim,
            description = description,
            geozone = geozone,
        )
        training.officers.append(current_user)
        pilots_dict = json.loads(pilots)
        
        for p in pilots_dict['attended']:
            m = Member.query.filter_by(id=p).first()
            #check if already 2 recent attended, and deduct a recent unadvised if it's not already zero.
            #(one recent unadvised deducted for every 3 trainings attended)
            #recent attended resets to zero whenever it reaches 3, and the effect applied to recent_unadvised
            if m.recent_attended > 1:   
                m.recent_attended = 0
                if m.absent_trainings > 0:
                    m.absent_trainings -=1
            m.recent_attended +=1        
            db.session.add(m)
            training.attendees.append(m)
        for p in pilots_dict['advised']:
            m = Member.query.filter_by(id=p).first()
            db.session.add(m)
            training.advisories.append(m) 
        for p in pilots_dict['missed']:
            m = Member.query.filter_by(id=p).first()
            m.recent_attended = 0
            m.absent_trainings += 1
            if m.absent_trainings > 2:
                m.warn = True
            db.session.add(m)
            training.nonadvisories.append(m)        
        db.session.add(training)
        db.session.commit()
        
        app.logger.info('Training with id {} has been created.'.format(training.id))

        return redirect(url_for('members.trainings'))

    members = Member.query.filter_by(active=True).all()
    member_data = {m.id:m.req_training for m in members}
    return render_template(
        'members/create_training.html', 
        members=members, 
        form=form, 
        data_json = json.dumps(member_data))


@bp.route('/training/<int:training_id>/edit', methods=('GET', 'POST'))
@login_required
@instructor_required
def edit_training(training_id):
    training = Training.query.filter_by(id=training_id).first()
    users = User.query.all()
    form = EditTrainingForm(request.form, obj=training)
    form.officer.choices= [(-1, 'No Change')] + [(u.id, u.username) for u in users if u.has_role('officer')]
    if form.validate_on_submit():
        form.populate_obj(training)
        officer_id = form.officer.data
        if officer_id != -1:
            officer = User.query.filter_by(id=officer_id).first()
            training.officers.append(officer)
        db.session.commit()
        app.logger.info('Training with id {} has been edited.'.format(training_id))
        return redirect(url_for('members.trainings'))
    return render_template('members/edit_training.html', form=form, t=training)


@bp.route('/delete_training/<int:training_id>', methods=['GET', 'POST'])
@login_required
@instructor_required
def delete_training(training_id):
    training = Training.query.filter_by(id=training_id).first()
    if request.method == 'POST':
        for pilot in training.nonadvisories:
            #account for the miscounting of total absent trainings
            pilot.absent_trainings -=1
            #correct the warn flag if removing this training 
            if pilot.absent_trainings < 3:
                pilot.warn = False
        db.session.delete(training)
        db.session.commit()
        
        app.logger.info('Training with id {} has been deleted.'.format(training_id))

        return redirect(url_for('members.trainings'))

    return render_template('members/delete_training.html', tr=training)


@bp.route('/trainings')
@login_required
def trainings():
    trainings = Training.query.all()
    return render_template('members/trainings.html', trainings=trainings)


@bp.route('/statistics')
@login_required
def statistics():
    training_stats = dict(
        wt = len(Training.query.filter_by(sim='wt').all()),
        dcs = len(Training.query.filter_by(sim='dcs').all()),
        il2 = len(Training.query.filter_by(sim='il2').all()),
        bms = len(Training.query.filter_by(sim='bms').all()),
    )
    
    total_trainings = sum(training_stats.values())
    training_stats['total_trainings'] = total_trainings 
    
    all_members = Member.query.filter_by(active=True).all()
    join_dates = [{'join_date':m.join_date} for m in all_members]
    join_graph = plots.join_date_graph(join_dates, date_key='join_date', )



    member_stats = dict(
        total_members = len(all_members),
    )

    rank_stats = {rank: len(Member.query.filter_by(pilot_rank=rank, active=True).all()) for rank in PilotRoles.ranks}

    script_players_per_platform, div_players_per_platform = plots.players_per_platform()
    script_main_sim_pie, div_main_sim_pie = plots.main_sim_pie()
    script_rank_pie, div_rank_pie = plots.rank_chart()
    script_attendance_graph, div_attendance_graph = plots.attendance_graph()

    

    return render_template(
        'members/statistics.html',
        training_stats = training_stats,
        rank_stats = rank_stats,
        member_stats = member_stats,
        script_players_per_platform=script_players_per_platform, 
        div_players_per_platform=div_players_per_platform,
        script_main_sim_pie=script_main_sim_pie, 
        div_main_sim_pie=div_main_sim_pie,
        script_rank_pie=script_rank_pie,
        div_rank_pie=div_rank_pie,
        script_attendance_graph=script_attendance_graph,
        div_attendance_graph=div_attendance_graph,

        join_graphJSON=join_graph)

@bp.route('/empire')
@login_required
def empire():
    graph = plots.world_map()
    return render_template('members/empire.html', graphJSON=graph,)

@bp.route('/discord', methods=['GET', 'POST'])
@login_required
@officer_required
def discord():
    btn_status = ''
    if request.method == "POST":
        if not current_user.has_role('officer'):
            return redirect(url_for('auth.denied'))
        app.logger.info('Discord bot is spooling up...')
        from x51.discordbot import exec_bot, member_outfile_path, roles_outfile_path
        app.logger.info(member_outfile_path)
        app.logger.info(roles_outfile_path)
        exec_bot()
        app.logger.info('Bot execution successful. Bot offline')
        btn_status = 'disabled'
    data = get_discord_json('members')
    if not data:
        return render_template('members/discord_inactive.html')
    graph = plots.join_date_graph(data)
    filtered_data=[]
    n_x51 = 0
    n_guests = 0
    n_wt = 0
    n_dcs = 0
    n_il2 = 0
    n_arma = 0
    n_bms = 0

    for m in data:
        if 'War Thunder' in m['roles']:
            m['wt'] = True
            n_wt+=1
        else:
            m['wt'] = False
        if 'DCS' in m['roles']:
            m['dcs'] = True
            n_dcs+=1
        else:
            m['dcs'] = False
        if 'IL-2' in m['roles']:
            m['il2'] = True
            n_il2+=1
        else:
            m['il2'] = False
        if 'Arma 3' in m['roles']:
            m['arma'] = True
            n_arma+=1
        if 'Falcon BMS' in m['roles']:
            m['bms'] = True
            n_arma+=1
        else:
            m['arma'] = False
        if '=X51= PILOTS' in m['roles']:
            m['x51'] = True
            n_x51+=1
        else:
            m['x51'] = False
            n_guests+=1

        filtered_data.append(m)
        
    stats = dict(
        n_x51 = n_x51,
        n_guests = n_guests,
        n_wt = n_wt,
        n_il2 = n_il2,
        n_arma = n_arma,
        n_dcs = n_dcs,
        n_bms = n_bms,)

    pie_script, pie_div = plots.discord_makeup(stats)
    return render_template(
        'members/discord.html',
        graphJSON=graph,
        members=filtered_data,
        stats=stats,
        pie_div = pie_div,
        pie_script = pie_script,
        btn_status = btn_status)

@bp.route('/discord_roles', methods=['GET', 'POST'])
@login_required
def discord_roles():
    btn_status = ''
    if request.method == "POST":
        if not current_user.has_role('officer'):
            return redirect(url_for('auth.denied'))
        app.logger.info('Discord bot is spooling up...')
        from x51.discordbot import exec_bot
        exec_bot()
        app.logger.info('Bot execution successful. Bot offline')
        btn_status = 'disabled'
    role_data = get_discord_json('roles')
    member_data = get_discord_json('members')
    if not role_data or not member_data:
        return render_template('members/discord_inactive.html')

    role_member_dict = dict()
    for role in role_data:
        role_member_dict[role] = [m['display_name'] for m in member_data if role in m['roles'] ]
    
    return render_template('members/discord_roles.html', data=role_member_dict)


################### SPEC TRAINING CATEGORIES ##############################
@bp.route('/add_spec', methods=('GET', 'POST'))
@login_required
@officer_required
def add_spec():
    form = NewSpecForm(request.form)
    if form.validate_on_submit():
        name = form.name.data
        spec_check = SpecTraining.query.filter_by(name=name).first()
        if spec_check:
            flash('{} already exists'.format(name), 'danger')
        else:
            spec = SpecTraining(
                name=name)
                
            db.session.add(spec)
            db.session.commit()
            app.logger.info('New Spec Training "{}" created successfully'.format(name))
            return redirect(url_for('members.specs'))
        
    return render_template('members/new_spec.html', form=form)

@bp.route('/specs')
@login_required
def specs():
    specs = SpecTraining.query.all()
    return render_template('members/specs.html', specs=specs)

@bp.route('/spec/<int:spec_id>', methods=['GET', 'POST'])
@login_required
@officer_required
def spec(spec_id):
    spec = SpecTraining.query.filter_by(id=spec_id).first()
    members = Member.query.filter_by(spec_training_id = spec_id, active=True).all()
    return render_template('members/spec.html', spec=spec, members = members)


@bp.route('/delete_spec/<int:spec_id>', methods=['GET', 'POST'])
@login_required
@officer_required
def delete_spec(spec_id):
    spec = SpecTraining.query.filter_by(id=spec_id).first()
    if request.method == 'POST':
        name = spec.name
        db.session.delete(spec)
        db.session.commit()
        app.logger.info('Spec "{}" has been deleted.'.format(name))
        return redirect(url_for('members.specs'))

    return render_template('members/delete_spec.html', spec=spec)