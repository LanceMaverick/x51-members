
from flask_wtf import FlaskForm
#from flask_security.forms import LoginForm
from wtforms.ext.sqlalchemy.orm import model_form
from wtforms import SubmitField, StringField, PasswordField, SelectField, SubmitField, TextAreaField, BooleanField, HiddenField, IntegerField, DecimalField, validators
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired
from wtforms.widgets import html5
from wtforms.validators import InputRequired

from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_uploads import UploadSet, IMAGES, DOCUMENTS

from pycountry import countries

from x51.models import Member, ModSyncVersion, Training, Award, SpecTraining
from x51.helpers import PilotRoles, GeoZones, MissionEntityTypes
from x51 import db
from x51.base.forms import field_helpers, my_strtobool

class LoginForm(FlaskForm):
    username = StringField(u'Username', [validators.required(), validators.length(max=120)])
    password = PasswordField(u'Password', [validators.required()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Sign In')
#class UserLoginForm(LoginForm):
#    def validate(self):
#        response = super(CustomLoginForm, self).validate()
#        return response
images = UploadSet('images', IMAGES)
archive_files = ['jpg', 'jpe', 'jpeg', 'png', 'gif', 'svg', 'bmp', 'pdf']

class LinkMemberForm(FlaskForm):
    member = SelectField('Member', coerce=int)
    user = SelectField('User', coerce=int)
    
    submit = SubmitField('Submit')

    
class NewTrainingForm(FlaskForm):
    date = field_helpers.date_picker()
    sim = field_helpers.all_platforms()
    geozone = field_helpers.geozone()
    description = TextAreaField(u'Description', [validators.optional(), validators.length(max=1000)])
    attendees = StringField(u'Attendees', [validators.required()])
    submit = SubmitField('Submit')
            
class NewMemberForm(FlaskForm):
    username = StringField(u'Username', [validators.required(), validators.length(max=100)])
    join_date = field_helpers.date_picker(label="Join Date")
    location = SelectField('Country', choices=[(c.name, c.name) for c in countries])
    geozone = field_helpers.geozone()
    wt = BooleanField('War Thunder')
    il2 = BooleanField('IL2')
    dcs = BooleanField('DCS: World')
    bms = BooleanField('Falcon BMS')
    arma = BooleanField('ARMA III')
    req_training = field_helpers.all_platforms()
    spec_training_id = SelectField (u'Specialisation', coerce=int)
    submit = SubmitField('Submit')


FullEditMemberForm = model_form(Member, base_class = FlaskForm, exclude=[
    'warn', 
    'attended', 
    'advised', 
    'missing',
    'attendees',
    'advisories',
    'nonadvisories',
    'user',
    'awards'])
FullEditMemberForm.pilot_rank = SelectField(u'Rank', choices = PilotRoles.form_ranks)
FullEditMemberForm.join_date = field_helpers.date_picker()
FullEditMemberForm.req_training = field_helpers.all_platforms()
FullEditMemberForm.spec_training_id = SelectField (u'Specialisation', coerce=int)
FullEditMemberForm.location = SelectField(u'Country', choices=[(c.name, c.name) for c in countries])
FullEditMemberForm.geozone = field_helpers.geozone()
FullEditMemberForm.wt = SelectField(u'War Thunder', choices=[(True, 'yes'), (False, 'no')], coerce=my_strtobool)
FullEditMemberForm.dcs = SelectField(u'DCS: World', choices=[(True, 'yes'), (False, 'no')], coerce=my_strtobool)
FullEditMemberForm.il2 = SelectField(u'IL2', choices=[(True, 'yes'), (False, 'no')], coerce=my_strtobool)
FullEditMemberForm.bms = SelectField(u'Falcon BMS', choices=[(True, 'yes'), (False, 'no')], coerce=my_strtobool)
FullEditMemberForm.arma = SelectField(u'ARMA', choices=[(True, 'yes'), (False, 'no')], coerce=my_strtobool)
FullEditMemberForm.on_reserve = SelectField(u'On Leave', choices=[(True, 'yes'), (False, 'no')], coerce=my_strtobool)
FullEditMemberForm.reserve_date = DateField('Date of leave (if pilot is on reserve)', format='%Y-%m-%d',validators=[validators.Optional()] )
FullEditMemberForm.active = SelectField(u'Active Membership', choices=[(True, 'yes'), (False, 'no')], coerce=my_strtobool)
FullEditMemberForm.submit = SubmitField('Submit')


EditTrainingForm = model_form(Training, base_class = FlaskForm, exclude=[
    'attendees',
    'advisories',
    'nonadvisories',
    'officers',
    'users'])
EditTrainingForm.sim = field_helpers.all_platforms()
EditTrainingForm.date = field_helpers.date_picker()
EditTrainingForm.geozone = field_helpers.geozone()
EditTrainingForm.description = TextAreaField(u'Description', [validators.optional(), validators.length(max=1000)])
EditTrainingForm.officer = SelectField('Logging Officer', coerce=int)
EditTrainingForm.submit = SubmitField('Submit')


#AwardFormBase = model_form(Award, base_class= FlaskForm, exclude = ['awards', 'icon_url'])

class NewAwardForm(FlaskForm):
    name = StringField(u'Name', [validators.required(), validators.length(max=120)])
    category = SelectField(u'Category', choices=[('General', 'General'), ('War Thunder', 'War Thunder'), ('IL-2', 'IL2'), ('DCS', 'DCS Combat'), ('Aerobatcs', 'Aerobatics'),  ('BMS', 'BMS')])
    description = TextAreaField(u'Description', [validators.optional(), validators.length(max=1000)])
    prize = TextAreaField(u'Prize Awarded', [validators.optional(), validators.length(max=1000)])
    award_img = FileField(u'Award Image', validators=[FileRequired(), FileAllowed(images, 'Only image files are allowed.')])
    submit = SubmitField('Submit')

class EditAwardForm(FlaskForm):
    name = StringField(u'Name', [validators.required(), validators.length(max=120)])
    category = SelectField(u'Category', choices=[('General', 'General'), ('War Thunder', 'War Thunder'), ('IL-2', 'IL2'), ('DCS', 'DCS Combat'), ('Aerobatcs', 'Aerobatics'),  ('BMS', 'BMS')])
    description = TextAreaField(u'Description', [validators.optional(), validators.length(max=1000)])
    prize = TextAreaField(u'Prize Awarded', [validators.optional(), validators.length(max=1000)])
    submit = SubmitField('Submit')

class EditAwardImgForm(FlaskForm):
    award_img = FileField(u'Award Image', validators=[FileRequired(), FileAllowed(images, 'Only image files are allowed.')])
    submit = SubmitField('Submit')

class ChooseAwardForm(FlaskForm):
    submit = SubmitField('Submit')

ROLES = [('','No Change'), ('member', 'member'), ('curator', 'curator'), ('instructor', 'instructor'), ('officer', 'officer'), ('admin', 'admin')]
#TODO: combine forms with mixin:
class NewUserForm(FlaskForm):
    username = StringField(u'Username', [validators.required(), validators.length(max=120)])
    password = StringField(u'Password', [validators.required()])
    tier = SelectField(u'Tier', choices=ROLES)
    member = SelectField('Member', coerce=int)
    submit = SubmitField('Submit')

class EditUserForm(FlaskForm):
    user = SelectField('User', coerce=int)
    password = StringField(u'Password')
    tier = SelectField(u'Tier', choices=ROLES)
    member = SelectField('Member', coerce=int)
    active = BooleanField('Active')
    submit = SubmitField('Submit')


class ActivitySearchForm(FlaskForm):
    all_dates = BooleanField('All Time', default="checked")
    start_date = field_helpers.date_picker(label="Start Date", required=False)
    end_date = field_helpers.date_picker(label="End Date", required=False)
    required = BooleanField('Required Training Only', default="checked")
    geozone = field_helpers.geozone(add_choices=[('All', 'All' )])
    sim = field_helpers.sim_platforms(add_choices=[('All', 'All' )])
    submit = SubmitField('Apply')

    #required_only = BooleanField('Calculate activity from required trainings only')
    

class AllActivityForm(FlaskForm):
    last_3_months = BooleanField('Last 90 Days')
    start_date = field_helpers.date_picker(label="Start Date", required=False)
    end_date = field_helpers.date_picker(label="End Date", required=False)
    submit = SubmitField('Apply')

############################
### Virtual Sortie Forms ###
############################

class NewSortieForm(FlaskForm):
    platform = field_helpers.sim_platforms(add_choices=[('arma', 'ARMA III' )])
    date = field_helpers.date_picker(label="Date of Sortie", required=True)
    nationality = field_helpers.sortie_nat()
    description = TextAreaField(u'Sortie Description', [validators.length(max=1000)])
    submit = SubmitField('Create')

class NewFlightForm(FlaskForm):
    air_kills = IntegerField('Number of air kills', widget=html5.NumberInput(min=0), default=0)
    assists = IntegerField('Number of assists', widget=html5.NumberInput(min=0), default=0)
    ground_kills = IntegerField('Number of ground kills', widget=html5.NumberInput(min=0), default=0)
    successes = IntegerField('Number of successful missions', widget=html5.NumberInput(min=0), default=0)
    failures = IntegerField('Number of failed missions', widget=html5.NumberInput(min=0), default=0)
    aircraft_losses = IntegerField('Number of aircraft lost', widget=html5.NumberInput(min=0, max=1), default=0)
    died = BooleanField('Check this if pilot died or was captured during this sortie')
    submit = SubmitField('Submit Combat Log')

class NewSortiePilotForm(FlaskForm):
    name = StringField('Name of pilot')
    nationality = field_helpers.sortie_nat()
    platform = field_helpers.sim_platforms(add_choices=[('arma', 'ARMA III' )])
    submit = SubmitField('Create')

class EditSortiePilotForm(FlaskForm):
    name = StringField('Name of pilot')
    submit = SubmitField('Update')

class NewMissionForm(FlaskForm):
    #name = StringField(u'Name', [validators.required(), validators.length(max=120)])
    category = SelectField(u'Category', choices=[('War Thunder', 'War Thunder'), ('IL-2', 'IL2'), ('DCS', 'DCS'), ('BMS', 'BMS'), ('Other', 'Other')])
    description = TextAreaField(u'Description', [validators.optional(), validators.length(max=1000)])
    briefing = FileField(u'Briefing PDF or pptx (optional)', validators=[FileAllowed(['pdf', 'pptx'], 'Only pdf and pptx files are allowed.')])
    mission = FileField(u'Mission File', validators=[FileRequired(), FileAllowed(['blk', 'miz', 'rar', 'zip', 'mission', 'msnbin'], 'Only blk, miz, msnbin, Mission, zip and rar files are allowed')])
    submit = SubmitField('Submit')

class NewArchiveForm(FlaskForm):
    description = TextAreaField(u'Description', [validators.optional(), validators.length(max=1000)])
    file = FileField(u'Archive File', validators=[FileRequired(), FileAllowed(archive_files, 'Only PDFs and images are allowed.')])
    submit = SubmitField('Submit')

class NewKbForm(FlaskForm):
    title = StringField(u'Mission Name', [validators.required(), validators.length(max=120)] )
    date = field_helpers.date_picker(label="Date of Mission", required=True)
    summary = TextAreaField(u'Mission Summary', [validators.required(), validators.length(max=500)])
    roe = TextAreaField(u'Rules of Enagement and SPINS', [validators.required(), validators.length(max=500)])
    submit = SubmitField('Submit')
    
class NewKbFlightForm(FlaskForm):
    name = StringField(u'Callsign', [validators.required(), validators.length(max=120)] )
    airframe = StringField(u'Airframe', [validators.required(), validators.length(max=120)] )
    task = StringField(u'Tasking', [validators.required(), validators.length(max=120)] )
    freq = DecimalField("Frequency (MHz AM)", validators=[validators.optional()], places=3)
    submit = SubmitField('Submit')

class NewKbEntityForm(FlaskForm):
    type = SelectField('Role or Type', choices=[ role for role in MissionEntityTypes.entities])
    name = StringField(u'Name or Callsign', [validators.required(), validators.length(max=120)] ) 
    freq = DecimalField("Frequency (MHz AM)", validators=[validators.optional()], places=3)
    rwy = StringField(u'Runway Heading', validators = [validators.optional(), validators.length(max=5)] )
    ils = DecimalField("ILS Frequency", validators=[validators.optional()], places=3)
    tacan = StringField(u'TACAN', validators = [validators.optional(), validators.length(max=5)] )
    alt = IntegerField("Altitude", validators=[validators.optional()])
    submit = SubmitField('Submit')

class NewSpecForm(FlaskForm):
    name = StringField(u'Name', [validators.required(), validators.length(max=120)] ) 
    submit = SubmitField('Submit')



class ModSyncForm(FlaskForm):
    major = IntegerField("Major", validators=[InputRequired()])
    minor = IntegerField("Minor", validators=[InputRequired()])
    patch = IntegerField("Patch", validators=[InputRequired()])
    submit = SubmitField('Submit')




