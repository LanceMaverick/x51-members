import os
import uuid
from flask import (
    Blueprint, 
    flash, 
    g, 
    redirect, 
    render_template,
    request,
    url_for,
    send_from_directory,
    Markup
)
import logging
from flask import current_app as app
import flask_mobility
from werkzeug.exceptions import abort
from werkzeug.datastructures import CombinedMultiDict
from werkzeug import secure_filename
from flask_security import login_required, current_user

from x51.auth import officer_required, admin_required
from x51.models import User, Kneeboard, KbFlight, KbEntity
from x51.forms import NewKbForm, NewKbFlightForm, NewKbEntityForm
from x51.kbgenerator import KbGen
from .database import db

bp = Blueprint('kneeboard', __name__, url_prefix='/kneeboard')

#index
@bp.route('/')
def index():
    # return list of all kneeboards
    kbs = Kneeboard.query.all()
    return render_template('kneeboard/index.html', kbs = kbs)


#add a new kneeboard
@bp.route('/new', methods = ['GET', 'POST'])
@login_required
def new_kb():
    form = NewKbForm()
    if request.method == 'POST':
        kb = Kneeboard(
            title = form.title.data,
            date = form.date.data,
            summary = form.summary.data,
            roe = form.roe.data,   
        )
        db.session.add(kb)
        db.session.commit()
        return redirect(url_for('kneeboard.kboard', kb_id=kb.id))
    return render_template('kneeboard/new_kb.html', form=form)

#view for a particular kneeboard
@bp.route('/kb/<int:kb_id>')
@login_required
def kboard(kb_id):
    kb = Kneeboard.query.filter_by(id=kb_id).first()
    return render_template(
        'kneeboard/kb.html',
        kb=kb)

#add a flight to a kneeboard
@bp.route('/kneeboard/<int:kb_id>/new_flight', methods = ['GET','POST'])
@login_required
def new_kb_flight(kb_id):
    kb = Kneeboard.query.filter_by(id=kb_id).first()
    
    form = NewKbFlightForm()
    if request.method == 'POST':
        flight = KbFlight(
            kb = kb,
            kb_id = kb_id,
            name = form.name.data,
            airframe = form.airframe.data,
            task = form.task.data,
            freq = form.freq.data
        )

        db.session.add(flight)
        db.session.commit()

        flash('New flight created!', 'success')
        return redirect(url_for('kneeboard.kboard', kb_id=kb_id))
    return render_template(
        'kneeboard/new_flight.html',
        form=form,
        kb=kb)

#add a entity to a kneeboard
@bp.route('/kneeboard/<int:kb_id>/new_entity', methods = ['GET','POST'])
@login_required
def new_kb_entity(kb_id):
    kb = Kneeboard.query.filter_by(id=kb_id).first()

    form = NewKbEntityForm()
    if request.method == 'POST':
        entity = KbEntity()
        form.populate_obj(entity)
        entity.kb = kb
        db.session.add(entity)

        db.session.commit()
        flash('New entity added!', 'success')
        return redirect(url_for('kneeboard.kboard', kb_id=kb_id))
    return render_template(
        'kneeboard/new_entity.html',
        form=form,
        kb=kb)

@bp.route('/kneeboard/entity/<int:entity_id>/delete', methods = ['GET','POST'])
@login_required
def delete_entity(entity_id):
    entity = KbEntity.query.filter_by(id=entity_id).first()
    kb_id = entity.kb.id
    if request.method =='POST':
        db.session.delete(entity)
        db.session.commit()
        flash('Entity Deleted', 'warning')
        return redirect(url_for('kneeboard.kboard', kb_id=kb_id))
    return render_template('kneeboard/delete_entity.html', entity=entity, kb_id=kb_id)

@bp.route('/kneeboard/flight/<int:flight_id>/delete', methods = ['GET','POST'])
@login_required
def delete_flight(flight_id):
    flight = KbFlight.query.filter_by(id=flight_id).first()
    kb_id = flight.kb.id
    if request.method =='POST':
        db.session.delete(flight)
        db.session.commit()
        flash('Flight Deleted', 'warning')
        return redirect(url_for('kneeboard.kboard', kb_id=kb_id))
    return render_template('kneeboard/delete_flight.html', flight=flight, kb_id=kb_id)


@bp.route('/kneeboard/<int:kb_id>/delete', methods = ['GET','POST'])
@login_required
def delete_kb(kb_id):
    kb = Kneeboard.query.filter_by(id=kb_id).first()
    if request.method =='POST':
        for entity in kb.entities:
            db.session.delete(entity)
        for flight in kb.flights:
            db.session.delete(flight)
        db.session.delete(kb)
        db.session.commit()
        flash('Mission Card Deleted', 'warning')
        return redirect(url_for('kneeboard.index', kb_id=kb_id))
    return render_template('kneeboard/delete_kb.html', kb=kb)
    
@bp.route('/kneeboard/<int:kb_id>/edit', methods = ['GET','POST'])
@login_required
def edit_kb(kb_id):
    kb = Kneeboard.query.filter_by(id=kb_id).first()
    form = NewKbForm(request.form, obj=kb)
    if request.method =='POST':
        form.populate_obj(kb)
        db.session.commit()
        flash('Mission Card Updated', 'success')
        return redirect(url_for('kneeboard.kboard', kb_id=kb_id))
    return render_template('kneeboard/edit_kb.html', kb=kb, form=form)

@bp.route('/kneeboard/<int:kb_id>/download', methods = ['GET'])
@login_required
def download_kb(kb_id):
    kb = Kneeboard.query.filter_by(id=kb_id).first()
    font = os.path.join(app.instance_path, 'fonts', 'BorgenBold.ttf')
    template = os.path.join(app.instance_path, 'images', 'kb_background.png')
    gen = KbGen(kb, template, font )
    gen.generate([app.config['UPLOAD_FOLDER'], 'kbs'])
    return send_from_directory(directory=gen.path, filename=gen.filename, as_attachment=True)
