import os
import sys
import json
import requests
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify, Response
)
from flask import current_app as app
from flask_cors import cross_origin
from x51.handlers import get_dcs_servers, get_discord_json
from x51.wtrss import gen_wtrss
from x51.config import DCSConfig, SRSConfig


basedir = os.path.realpath(os.path.dirname(__file__))

bp = Blueprint('api', __name__, url_prefix='/api')

def request_log(request):
    app.logger.info('API REQUEST: {} - {} - {}'.format(
        request.endpoint,
        request.method,
        request.environ['REMOTE_ADDR']))

@bp.route('/discord/stats', methods=['GET'])
@cross_origin()
def discord_stats():
    request_log(request)
    return jsonify(get_discord_json())



@bp.route('/servers/dcs', methods=['GET'])
@cross_origin()
def dcs_servers():
    request_log(request)
    DCS_USER = os.environ.get('DCS_USER')
    DCS_PW = os.environ.get('DCS_PW')
    if not DCS_USER or not DCS_PW:
        app.logger.info('Env vars undefined. Getting DCS credidentials from config')
 #       try:
        DCS_USER = DCSConfig.DCS_USER
        DCS_PW = DCSConfig.DCS_PW
#        except ImportError:
#            pass #TODO log it
#        except AttributeError:
#            pass #TODO log it

    server_list = get_dcs_servers(DCS_USER, DCS_PW)
    
    return jsonify(server_list)


@bp.route('/srs/setip', methods=['PUT'])
def set_ip():
    request_log(request)
    
    data = json.loads(request.data)
    key = data['key']
    ip = data['ip']
    if key != SRSConfig.ADMIN_KEY:
        return Response('Access Denied', 401, {'WWW-Authenticate':'Basic realm="Login Required"'})
    elif not ip or not ip.replace('.', '').isdigit():
       return Response('Cannot interpret data', 409) 
    else:
        headers = {"Authorization": "Bearer " + SRSConfig.DO_KEY}
        r = requests.put(SRSConfig.SRS_RECORD_PUT_URL, data= {"data":ip}, headers=headers)
        
        return Response('Attempted IP update', r.status_code)

@bp.route('feeds/war-thunder')
def wt_feed():
    feed = gen_wtrss(app.instance_path)
    app.logger.info(feed)
    return Response(feed.xml, mimetype='text/xml')

