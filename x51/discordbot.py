import os
import sys
import asyncio
import json
import discord
import logging
from x51.config import DiscordConfig

basedir = os.path.realpath(os.path.dirname(__file__))
member_outfile_path = os.path.join(basedir, "data",  "discord_members.json")
roles_outfile_path = os.path.join(basedir, "data",  "discord_roles.json")

def exec_bot():
    logging.info("EXECUITING DISCORD BOT")

    asyncio.get_child_watcher()
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    intents = discord.Intents.default()
    intents.members = True
    client = discord.Client(loop=loop, intents=intents)

    #@client.event
    #async def on_member_join(member):
    #    await save_stats(client)
    
    @client.event
    async def on_ready():
        await save_stats(client)
        
    async def save_stats(client):
        for g in client.guilds:
            if g.id == DiscordConfig.DISCORD_SERVER_ID:
                stats= []
                roles= []
                for m in g.members:
                    m_dict = dict(
                            joined_at = m.joined_at.strftime('%m/%d/%Y-%H:%M:%S'),
                            nick = m.nick,
                            display_name = m.display_name,
                            top_role = str(m.top_role),
                            avatar_url = str(m.avatar_url),
                            roles = [str(r) for r in m.roles])

                    stats.append(m_dict)
                for r in g.roles:
                    roles.append(str(r))

                logging.info(stats)
                logging.info(roles)
                with open(member_outfile_path, 'w') as outfile:
                    json.dump(stats, outfile)
                with open(roles_outfile_path, 'w') as outfile:
                    json.dump(roles, outfile)
                    
                await client.logout()
   
    client.run(DiscordConfig.DISCORD_BOT_TOKEN)



