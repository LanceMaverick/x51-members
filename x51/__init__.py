import os
from flask import Flask
from flask_assets import Environment, Bundle
from flask_cors import CORS
from flask_bootstrap import Bootstrap
#from flask_sqlalchemy import SQLAlchemy
from flask_scss import Scss
#from flask_alembic import Alembic
from flask_migrate import Migrate
from flask_login import LoginManager
from .database import db
try:
    from .config import Config
except ModuleNotFoundError as e:
    raise ModuleNotFoundError("No config file. You must copy config.py.example to config.py and set it up", e)

#test_config = ''
basedir = os.path.abspath(os.path.dirname(__file__))

def create_app(test_config=None):
# create and configure the app
    app = Flask(__name__, instance_relative_config=True, instance_path=os.path.join(basedir, 'static/instance/'))
    cors = CORS(app)
    app.config['CORS_HEADERS'] = 'Content-Type'


    assets = Environment(app)
    assets.url = app.static_url_path
    scss = Bundle(
        '_variables.scss',
        'app.scss',
        'icons.scss',
        'login.scss',
        'cookiebar.scss',
        filters='pyscss',
        output='all.css')
    assets.register('scss_all', scss)
    
    app.config.from_mapping(
        SECRET_KEY=Config.FLASK_KEY,
        UPLOAD_FOLDER = os.path.join(app.instance_path, 'uploads'),
        SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'app.db'),
        SQLALCHEMY_TRACK_MODIFICATIONS = False,
        SECURITY_PASSWORD_HASH = 'bcrypt',
        SECURITY_PASSWORD_SALT=Config.SALT,
        SECURITY_REMEMBER_SALT = Config.REMEMBER_SALT,
        SECURITY_USER_IDENTITY_ATTRIBUTES = 'username',
        SECURITY_LOGIN_URL = '/auth/login',
        MAX_CONTENT_LENGTH = 25 * 1024 * 1024
    )
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    Bootstrap(app)
#alembic.init_app(app)
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)
      # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path, exist_ok=True)
        os.makedirs(os.path.join(app.instance_path, '/images/uploads'), exist_ok=True)

    except OSError:
        pass

#db init
#manager = Manager(app)
#manager.add_command('db', MigrateCommand)
       
#setup instance folder for uploads

#auth bp register
    from . import auth
    app.register_blueprint(auth.bp)

#admin bp register
    from . import admin
    app.register_blueprint(admin.bp)

#patreon bp register
    from . import patreon
    app.register_blueprint(patreon.bp)

#member bp register
    from . import members
    app.register_blueprint(members.bp)
    app.add_url_rule('/', endpoint='index')

#awards bp register
    from . import awards
    app.register_blueprint(awards.bp)

#api bp register
    from . import api
    app.register_blueprint(api.bp, prefix='/api')

#fileapi bp register
    from . import fileapi
    app.register_blueprint(fileapi.bp, prefix='/fileapi')

#public bp register
    from . import public
    app.register_blueprint(public.bp)

#sortie bp register
    from . import sorties
    app.register_blueprint(sorties.bp)

#missions bp register
    from . import missions
    app.register_blueprint(missions.bp)

#archive bp register
    from . import archives
    app.register_blueprint(archives.bp)

#archive bp register
    from . import kneeboard
    app.register_blueprint(kneeboard.bp)  

#init db
    db.init_app(app)
    from x51 import models
    migrate = Migrate(app, db)

    

#init login manager
    login_manager = LoginManager()
    login_manager.init_app(app)
    login_manager.login_view = 'auth.login'
    #test only TODO
    @login_manager.user_loader
    def load_user(user_id):
        return None

#set up flask security
    from flask_security import Security, SQLAlchemyUserDatastore
    user_datastore = SQLAlchemyUserDatastore(db, models.User, models.Role)
    security = Security()
    security.init_app(app, user_datastore)
    security.login_view = 'auth.login'
    
##    @app.before_first_request
##    def before_first_request():

#global template functions
    def nl2br(s):
        if not s:
            return s
        else:
            return s.replace('\n', '<br>')

    app.jinja_env.globals.update(nl2br=nl2br)

    return app

#run external data handlers
#import threading
#from . import discordbot
#DCS_USER = os.environ.get('DCS_USER')
#DCS_PW = os.environ.get('DCS_PW')
    
if __name__ == '__main__':
    

    #run main app
    app = create_app()
    app.run()
