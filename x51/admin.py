import functools
import json
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash
from flask_security import login_user, logout_user, current_user, login_required
from flask_security import SQLAlchemyUserDatastore, utils

from flask import current_app as app

from x51.auth import officer_required, admin_required
from x51.models import Member, ModSyncVersion, User, Role
from x51.forms import LinkMemberForm, ModSyncForm, NewUserForm, EditUserForm
from .database import db

bp = Blueprint('admin', __name__, url_prefix='/admin')

#TODO refactor for common functions
user_datastore = SQLAlchemyUserDatastore(db, User, Role)

@bp.route('/')
@admin_required
def administer():
    users = User.query.all()
    return render_template('admin/admin.html', users=users)

@bp.route('/', methods=('GET', 'POST'))
@admin_required
def link():
    #get list of all members and all users from db
    members = Member.query.all()
    users = User.query.all()
    
    form = LinkMemberForm(request.form)
    form.member.choices= [(m.id, m.username) for m in members]
    form.user.choices =  [(u.id, u.username) for u in users]

    
    
    if request.method =='POST':
        member = Member.query.filter_by(id=form.member.data).first()
        user = User.query.filter_by(id=form.user.data).first()
        user.member = member
        db.session.commit()
        flash('Member {} linked with User {}'.format(member.username, user.username), 'success')

    return render_template('admin/link_accounts.html', form=form)

def role_tiers(tier):
    if tier == '':
        return []
    tiers = dict(
        member = ['member'],
        curator = ['member', 'curator'],
        instructor = ['member', 'instructor'],
        officer = ['member', 'officer'],
        admin = ['member', 'officer', 'admin']
    )
    try:
        return tiers[tier]
    except KeyError:
        app.logger.error('role tier {} does not exist'.format(tier))
@bp.route('/new_user', methods=('GET', 'POST'))
@admin_required
def new_user():
    members = Member.query.all()
    form = NewUserForm(request.form)
    form.member.choices= [(-1, 'None')] + [(m.id, m.username) for m in members]
    if form.validate_on_submit():
        username = form.username.data.lower()
        password = form.password.data
        tier = form.tier.data
        member_id = form.member.data
        if not user_datastore.get_user(username):
            user = user_datastore.create_user(username=username, password=utils.hash_password(password))
            for role in role_tiers(tier):
                user_datastore.add_role_to_user(user, role)
            
            if member_id != -1:
                member = Member.query.filter_by(id=member_id).first()
                user.member = member

            db.session.commit()
            flash('User "{}" created with roles: [{}]. Temporary password: {}'.format(
                username, 
                ', '.join([r.name for r in user.roles]),
                password
                ), 'success')
            return redirect(url_for('admin.administer'))
        else:
            flash('User "{}" already exists'.format(username), 'danger')

    return render_template('admin/user.html', form=form)

@bp.route('/edit_user', methods=('GET', 'POST'))
@admin_required
def edit_user():
    users = User.query.all()
    active_map = json.dumps({u.id:u.active for u in users})
    members = Member.query.all()
    form = EditUserForm(request.form)
    form.user.choices =  [(u.id, u.username) for u in users]
    form.member.choices= [(-1, 'No Change')] + [(m.id, m.username) for m in members]

    if form.validate_on_submit():
        user_id = form.user.data
        password = form.password.data
        tier = form.tier.data
        member_id = form.member.data
        active = form.active.data

        user = User.query.filter_by(id=user_id).first()
        
        if password:
            user.set_password(password)
            user.confirmed_at = None

        new_roles = role_tiers(tier)
        if new_roles:
            user.roles = []
            for role in role_tiers(tier):
                user_datastore.add_role_to_user(user, role)
        
        if member_id != -1:
            member = Member.query.filter_by(id=member_id).first()
            user.member = member

        user.active = active
        db.session.commit()
        flash('User "{}" with roles [{}] successfully edited'.format(
            user.username,
            ', '.join([r.name for r in user.roles])
            ), 'success')
        if password:
            flash('Password has been temporarily reset to: {}'.format(password), 'success')
        return redirect(url_for('admin.administer'))

    return render_template('admin/user.html', form = form, active_map = active_map)


####################

@bp.route('/modsync')
@admin_required
def modsync():
    vers = ModSyncVersion.query.all()
    return render_template('admin/modsync.html', vers=vers)
    

@bp.route('/add_ver', methods=('GET', 'POST'))
@admin_required
def add_ms_ver():
    form = ModSyncForm(request.form)
    if form.validate_on_submit():

        ModSyncVersion.query.delete()
        ver = ModSyncVersion(
            major=form.major.data,
            minor = form.minor.data,
            patch = form.patch.data)
            
        db.session.add(ver)
        db.session.commit()
        app.logger.info('New ModSync version "{}.{}.{}" created successfully'.format(form.major, form.minor, form.patch))
        return redirect(url_for('admin.modsync'))
        
    return render_template('admin/new_modsync.html', form=form)


@bp.route('/delete_ver/<int:ver_id>', methods=['GET', 'POST'])
@admin_required
def delete_modsync_version(ver_id):
    ver = ModSyncVersion.query.filter_by(id=ver_id).first()
    if request.method == 'POST':
        db.session.delete(ver)
        db.session.commit()
        app.logger.info('Version deleted.')
        return redirect(url_for('admin.modsync'))

    return render_template('admin/delete_ver.html', ver=ver)
