from distutils.util import strtobool
from flask_wtf import FlaskForm
from wtforms import SubmitField, StringField, PasswordField, SelectField, SubmitField, TextAreaField, BooleanField, HiddenField, IntegerField, validators
from wtforms.fields.html5 import DateField
from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_uploads import UploadSet, IMAGES, DOCUMENTS
from x51.helpers import PilotRoles, GeoZones

"""
In the future, make some base form classes here 
to inherit from to avoid repetition
"""
#IF YOU WANT SOMETHING DOING RIGHT
def my_strtobool(x):
    if type(x) == bool:
        return x
    else: 
        return strtobool(x)
#DO IT YOURSELF

class field_helpers:
    def all_platforms(add_choices = []):
        return SelectField(u'Platform', choices = add_choices + [('wt', 'War Thunder'), ('il2', 'IL2'), ('dcs', 'DCS Combat'), ('aero', 'Aerobatics'), ('bms', 'Falcon BMS'),])
    def sim_platforms(add_choices = []):
        return SelectField(u'Platform', choices= add_choices + [('wt', 'War Thunder'), ('il2', 'IL2'), ('dcs', 'DCS'), ('bms', 'Falcon BMS'),])
    def sortie_nat(add_choices = []):
        #return SelectField(u'Allegiance of Pilot', choices= add_choices + [('de', 'German'), ('gb', 'British'), ('ru', 'Russian'), ('us', 'American')])
        return SelectField(u'Allegiance', choices= add_choices + [('red', 'Allies'), ('blue', 'Axis')])
    def geozone(add_choices = []):
        return SelectField (u'Time Zone Group', choices = add_choices + [(k, "{} ({})".format(k, v)) for k, v in GeoZones.zones.items()]) 
    def date_picker(label="Date", required=True):
        if required:
            return DateField(label, [validators.required()], format='%Y-%m-%d' )
        else:
            return DateField(label, [], format='%Y-%m-%d' )
    
        