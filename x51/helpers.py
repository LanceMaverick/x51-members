import os
from pathlib import Path
import json
import uuid
from werkzeug import secure_filename
from flask import current_app as app
from flask import flash
from x51.models import Member, Training



def map_trainings_pilots(tr):
    pilots_json = json.loads(tr.attendees)
    pilots = dict(
        attended = [Member.query.filter_by(id=p).first() for p in pilots_json['attended']],
        advised = [Member.query.filter_by(id=p).first() for p in pilots_json['advised']],
        missed = [Member.query.filter_by(id=p).first() for p in pilots_json['missed']]
    )
    return [tr, pilots]

class PilotRoles:
    ranks = [
        'CADET',
        'AIRMAN BASIC',
        'AIRMAN',
        'AIRMAN 1st CLASS',
        'SENIOR AIRMAN',
        'MASTER SERGEANT',
        'VETERAN (retired)',
        '2nd LIEUTENANT',
        '1st LIEUTENANT',
        'CAPTAIN',
        'COLONEL',
        'COMMANDER'
    ]
    form_ranks = [(x,x) for x in ranks]

class Platforms:
    sim_keys = [
        'wt',
        'il2',
        'dcs',
        'bms',
    ]
    keys = sim_keys + ['arma']

    sim_names = [
        'War Thunder',
        'IL2',
        'DCS: World',
        'Falcon BMS',
    ]
    names = sim_names + ['ARMA 3']

    sims = dict(zip(sim_keys, sim_names))
    platforms = dict(zip(keys, names))


class GeoZones:
    #Splitting into two TZ zones
    zones = {
        'TZ1': 'Europe/Mid-West Asia/Africa',
        'TZ2': 'Americas/East Asia/Oceana',
    }

class MissionEntityTypes:
    #for kneeboard entity form
    entities = [
        ('airbase', 'AIRBASE'),
        ('farp', 'FARP'),
        ('tanker', 'TANKER'),
        ('controller', 'CONTROLLER')
    ]

def in_date_range(date, start_date, end_date):
    return start_date <= date <= end_date

def attrcheck(obj, attrs):
    result = []
    for attr in attrs:       
        if attr[1] == 'All':
            result.append(True)
        else:
            result.append(getattr(obj, attr[0]) == attr[1])
    
    return all(result)

def filter_query_list(obj, attrs, start_date, end_date):
    return [t for t in obj 
    if in_date_range(t.date, start_date, end_date)
    and attrcheck(t, attrs)]

def calc_member_activity(
    member,
    start_date,
    end_date,
    geozone,
    sim):

    training_filter = dict()
    if geozone != 'All':
        training_filter['geozone'] = geozone
    if sim != 'All':
        training_filter['sim'] = sim

    trainings = [
        t for t in Training.query.filter_by(**training_filter).all() 
        if in_date_range(t.date, start_date, end_date)]

    n_all_trainings = len(trainings)
    
    result = {}
    for a in ['attended', 'advised', 'missing']:
        result[a] = filter_query_list(
            getattr(member, a),
            [['geozone', geozone], ['sim', sim]],
            start_date,
            end_date)

    result['n_user_attended'] = len(result['attended'])
    result['n_all_trainings'] = n_all_trainings
    try:
        result['attendance'] = 100*result['n_user_attended']/result['n_all_trainings']
    except ZeroDivisionError:
        result['attendance'] = 0
    return result 
    

def calc_all_activity(members, start_date, end_date):
    results = []
    for m in members:
        #check member started at least as early as the start date
        #otherwise exclude
        if m.join_date > start_date:
            app.logger.info(m.username)
            continue
        
        attended = [tr for tr in m.attended if in_date_range(tr.date, start_date, end_date)]
        advised = [tr for tr in m.advised if in_date_range(tr.date, start_date, end_date)]
        missing = [tr for tr in m.missing if in_date_range(tr.date, start_date, end_date)]
        
        n_total = len(advised)+len(missing)+len(attended)

        #check they have at least been logged as
        #advised, missed or attended more than 
        #oncer per fortnight to exclude members on leave
        # or special status
        if n_total < (end_date-start_date).days/14:
            continue

        try:
            fom = len(attended)/(n_total)
        except ZeroDivisionError:
            fom = 0  

        results.append(dict(
        member = m,
        n_user_attended = len(attended),
        n_all_trainings = n_total,
        activity = fom*100,
        ))
    
    return sorted(results, key=lambda k: k['activity'], reverse=True)

def get_upload_fpath(fname, folder = '', check_path=True):
    secure_fname = secure_filename(fname)
    ufilename = uuid.uuid4().hex+secure_fname
    save_folder = os.path.join(app.config['UPLOAD_FOLDER'], folder)
    if check_path:
        #check folder exists. Make if not
        Path(save_folder).mkdir(parents=True, exist_ok=True)
        app.logger.info("Instance path {} did not exist. Creating it now.".format(save_folder))
    save_path = os.path.join(save_folder, ufilename)
    return ufilename, save_path

def get_file_location(fname, folder = ''):
    path = os.path.join(app.config['UPLOAD_FOLDER'], folder, fname)
    return path

def delete_upload(fname, folder = ''):
    path = get_file_location(fname, folder)
    try:
        os.remove(path)
    except FileNotFoundError as e:
        app.logger.error('Cannot find file {} for deletion: {}'.format(path, e))
    else:
        app.logger.info('Deleting upload file {}'.format(path))

def flash_errors(form):
    for field, errors in form.errors.items():
        for err in errors:
            flash(u"Error: {} - {}".format(
                    getattr(form, field).label.text,
                    err),
            'danger')
        




    
    

    