import os
import sys
import json
import requests

import re

from flask import Flask, request, jsonify, make_response, Blueprint
from flask import current_app as app
from flask_cors import cross_origin
from x51.models import User, ModSyncVersion
from x51.auth import token_required
from x51 import storage
from x51.api import request_log
import uuid
import jwt
import datetime

bp = Blueprint('fileapi', __name__, url_prefix='/fileapi')



@bp.route('/login', methods=['GET', 'POST'])  
def login_user():
    request_log(request)
    # log the user in via the API by returning an
    # auth token if credidentials are correct

    ver = request.args.get('ver')
    
    if ver is None:
        return make_response('version number not supplied', 401, {'WWW.Authentication': 'Basic realm: "login required"'})

    try:
        int(ver.strip().replace('.', ''))
        vernum = [int(x) for x in ver.strip().split('.')]
    except:
        return make_response('invalid format for version number', 400, {'WWW.Authentication': 'Basic realm: "login required"'})

    accepted_version = ModSyncVersion.query.first()

    if not accepted_version.acceptable(vernum):
        return make_response('The version of this tool is too old. Please update to the latest version', 401, {'WWW.Authentication': 'Basic realm: "login required"'})

    auth = request.authorization   

    if not auth or not auth.username or not auth.password:  
        return make_response('incomplete auth data', 401, {'WWW.Authentication': 'Basic realm: "login required"'})    

    user = User.query.filter_by(username=auth.username.lower()).first()
  
    if user is None or not user.check_password(auth.password):
        return make_response('authentication failed',  401, {'WWW.Authentication': 'Basic realm: "login required"'})

    token = jwt.encode({'username': user.username, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=360)}, app.config['SECRET_KEY'])  
    return jsonify({'token' : token.decode('UTF-8')}) 


@bp.route('/validate-token', methods=['GET'])  
@token_required
def validate_token(current_user):
    request_log(request)
    if current_user.has_role('curator') or current_user.has_role('officer'):
        message = 'curator'
    else:
        message = 'member'
    return make_response(jsonify({'message': message}), 200)
    

@bp.route('/get-file-list', methods=['GET'])  
@token_required
def get_file_list(current_user):
    request_log(request)
    try:
        files = storage.get_file_list('x51sqn')
    except Exception as e:
        app.logger.error(e)
        return make_response(jsonify(dict(message="request failed")), 400)
    return jsonify(dict(message="success", files=files))

@bp.route('/get-detailed-file-list', methods=['GET'])  
@token_required
def get_detailed_file_list(current_user):
    request_log(request)
    prefix = request.args.get('prefix')
    try:
        files = storage.get_detailed_file_list('x51sqn', prefix)
    except Exception as e:
        app.logger.error(e)
        return make_response(jsonify(dict(message="request failed")), 400)
    return jsonify(dict(message="success", files=files))

@bp.route('/get-upload-token', methods=['GET'])  
@token_required
def get_upload_token(current_user):
    request_log(request)
    if not current_user.has_role('curator') and not current_user.has_role('officer'):
        return jsonify(dict(message="insufficient user privelages"))    
    
    file_object = request.args.get('object')
    if not file_object:
        return jsonify(dict(message="no object parameter specified"))
    try:
        url = storage.get_presigned_upload_url(
            file_object,
            'x51sqn',
            10000)
    except Exception as e:
        app.logger.error(e)
        return jsonify(dict(message="request failed"))
    return jsonify(dict(message="success", url=url))

@bp.route('/delete-file', methods=['GET'])  
@token_required
def delete_file(current_user):
    request_log(request)
    if not current_user.has_role('curator') and not current_user.has_role('officer'):
        return jsonify(dict(message="insufficient user privelages"))    
    
    file_object = request.args.get('object')
    
    if not file_object:
        return jsonify(dict(message="no object parameter specified"))
    try:
        fo = file_object[1:] if file_object[:1] == "/" else file_object
        app.logger.info("file api: attempting to delete", fo)
        resp = storage.delete_object(fo, 'x51sqn')
    except Exception as e:
        app.logger.error(e)
        return jsonify(dict(message="request failed"))
    return jsonify(dict(message="success"))

@bp.route('/test', methods=['GET'])  
def test():
    request_log(request)
    return jsonify(dict(message="hello world!"))


#DCS#

@bp.route('/dcs/get-mods', methods=['GET'])  
@token_required
def get_dcs_mods(current_user):
    request_log(request)
    prefix = request.args.get('mod-type')
    if prefix and prefix not in ['aircraft', 'tech', 'services']:
        return make_response(jsonify(dict(message="request failed - mod-type does not exist.")), 400)
    try:
        files = storage.get_dcs_mods(prefix)
    except Exception as e:
        app.logger.error(e)
        return make_response(jsonify(dict(message="request failed"), 400))
    return jsonify(dict(message="success", files=files))

@bp.route('/dcs/get-mod', methods=['GET'])  
@token_required
def get_dcs_mod(current_user):
    request_log(request)
    mod_type = request.args.get('mod-type')
    mod = request.args.get('mod')

    if not mod or not mod_type or mod_type not in ['aircraft', 'tech', 'services']:
        app.logger.error(mod, mod_type)
        return make_response(jsonify(dict(message="request failed - check parameters")), 400)

    try:
        files = storage.get_dcs_mod(mod_type, mod)
    except Exception as e:
        app.logger.error(e)
        return jsonify(dict(message="request failed"))
    return jsonify(dict(message="success", files=files))

@bp.route('/dcs/delete-mod', methods=['GET'])  
@token_required
def delete_dcs_mod(current_user):
    request_log(request)
    if not current_user.has_role('curator') and not current_user.has_role('officer'):
        return jsonify(dict(message="insufficient user privelages"))

    mod = request.args.get('mod')
    mod_type = request.args.get('mod-type')
    if any(
        [mod is None,
        mod_type is None,
        mod_type == '',
        mod == '',
        mod_type not in ['aircraft', 'tech', 'services']]):
        return make_response(jsonify(dict(message="request failed - check parameters")), 400)
    
    try:
        storage.delete_dcs_mod(mod_type, mod)
    except Exception as e:
        app.logger.error(e)
        return make_response(jsonify(dict(message="request failed"), 400))
    return jsonify(dict(message="success")) 


@bp.route('/dcs/get-liveries', methods=['GET'])  
@token_required
def get_dcs_liveries(current_user):
    request_log(request)
    aircraft = request.args.get('aircraft')

    if not aircraft:
        app.logger.error(aircraft)
        return make_response(jsonify(dict(message="request failed - check parameters")), 400)
    try:
        files = storage.get_dcs_liveries(aircraft)
    except Exception as e:
        app.logger.error(e)
        return make_response(jsonify(dict(message="request failed"), 400))
    return jsonify(dict(message="success", files=files))


@bp.route('/dcs/get-livery-aircraft', methods=['GET'])  
@token_required
def get_dcs_livery_aircraft(current_user):
    request_log(request)
    
    try:
        files = storage.get_dcs_livery_aircraft()
    except Exception as e:
        app.logger.error(e)
        return jsonify(dict(message="request failed"))
    return jsonify(dict(message="success", files=files))

@bp.route('/dcs/get-livery', methods=['GET'])  
@token_required
def get_dcs_livery(current_user):
    request_log(request)
    aircraft = request.args.get('aircraft')
    livery = request.args.get('livery')

    if not aircraft or not livery:
        app.logger.error(aircraft, livery)
        return make_response(jsonify(dict(message="request failed - check parameters")), 400)
    try:
        files = storage.get_dcs_livery(aircraft, livery)
    except Exception as e:
        app.logger.error(e)
        return jsonify(dict(message="request failed"))
    return jsonify(dict(message="success", files=files))

@bp.route('/dcs/delete-livery', methods=['GET'])  
@token_required
def delete_dcs_livery(current_user):
    request_log(request)
    if not current_user.has_role('curator') and not current_user.has_role('officer'):
        return jsonify(dict(message="insufficient user privelages"))

    aircraft = request.args.get('aircraft')
    livery = request.args.get('livery')
    
    if any(
        [livery is None,
        aircraft is None,
        aircraft == '',
        livery == '',
        ]):
        return make_response(jsonify(dict(message="request failed - check parameters")), 400)
    
    try:
        storage.delete_dcs_livery(aircraft, livery)
    except Exception as e:
        app.logger.error(e)
        return make_response(jsonify(dict(message="request failed"), 400))
    return jsonify(dict(message="success"))

@bp.route('/dcs/delete-livery-aircraft', methods=['GET'])  
@token_required
def delete_dcs_livery_aircraft(current_user):
    request_log(request)
    if not current_user.has_role('curator') and not current_user.has_role('officer'):
        return jsonify(dict(message="insufficient user privelages"))

    aircraft = request.args.get('aircraft')
    
    if any(
        [aircraft is None,
        aircraft == '',
        ]):
        return make_response(jsonify(dict(message="request failed - check parameters")), 400)
    
    try:
        storage.delete_dcs_livery_aircraft(aircraft)
    except Exception as e:
        app.logger.error(e)
        return make_response(jsonify(dict(message="request failed"), 400))
    return jsonify(dict(message="success")) 