import os
import uuid
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, send_from_directory
)
from flask import current_app as app
import flask_mobility
from werkzeug.exceptions import abort
from werkzeug.datastructures import CombinedMultiDict
from werkzeug import secure_filename
from flask_security import login_required, current_user

from x51.auth import officer_required, admin_required
from x51.models import Member, Training, User, Award
from x51.forms import NewAwardForm, EditAwardForm, EditAwardImgForm
from .database import db
from .helpers import get_upload_fpath


bp = Blueprint('awards', __name__, url_prefix='/awards')

basedir = os.path.dirname(os.path.abspath(__file__))

@bp.route('/awards')
@login_required
def awards():
    awards = Award.query.all()
    app.logger.info(len(awards))
    for award in awards:
        app.logger.info(award.icon_url)
    return render_template('awards/awards.html', awards=awards)

@bp.route('/award/<int:award_id>')
@login_required
def award(award_id):
    award = Award.query.filter_by(id=award_id).first()
    return render_template('awards/award.html', award=award)

@bp.route('/create_award/', methods=['GET', 'POST'])
@officer_required
def add_award():
    form = NewAwardForm()
    if request.method == 'POST':
        fname = form.award_img.data.filename
        award_ufname, award_fpath = get_upload_fpath(fname, folder='images/awards')
        form.award_img.data.save(award_fpath) #save image
        award = Award(
            name = form.name.data,
            category = form.category.data,
            description = form.description.data,
            prize = form.prize.data,
            icon_url = award_ufname)
        db.session.add(award)
        db.session.commit()
        app.logger.info('Award with id {} has been created.'.format(award.id))
        return redirect(url_for('awards.awards'))
    return render_template('awards/add_award.html', form=form)

@bp.route('/award/<int:award_id>/edit', methods=('GET', 'POST'))
@login_required
@officer_required
def edit_award(award_id):
    award = Award.query.filter_by(id=award_id).first()
    form = EditAwardForm(request.form, obj=award)
    if form.validate_on_submit():
        form.populate_obj(award)
        db.session.commit()
        app.logger.info('Award with id {} has been edited.'.format(award.id))
        return redirect(url_for('awards.awards'))
    return render_template('awards/edit_award.html', form=form, award_name = award.name, img=False)

@bp.route('/award/<int:award_id>/edit/image', methods=('GET', 'POST'))
@login_required
@officer_required
def edit_award_img(award_id):
    award = Award.query.filter_by(id=award_id).first()
    form = EditAwardImgForm()
    if request.method == 'POST':
        fname = form.award_img.data.filename
        award_ufname, award_fpath = get_upload_fpath(fname, folder='images/awards')
        form.award_img.data.save(award_fpath) #save image
        award.icon_url = award_ufname
        db.session.commit()
        app.logger.info('Award with id {} image changed.'.format(award.id))
        return redirect(url_for('awards.awards'))
    return render_template('awards/edit_award.html', form=form, award_name = award.name, img=True)