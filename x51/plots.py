import sys
from collections import Counter
from math import pi
import json
from datetime import datetime

import pandas as pd


from bokeh.plotting import figure, show
from bokeh.embed import components
from bokeh.plotting import figure
from bokeh.palettes import Blues, Category20c, Set3
from bokeh.transform import cumsum

import plotly
import plotly.plotly as py

from pycountry import countries

from flask import current_app as app

from x51.helpers import PilotRoles
from x51.models import Member
from x51.models import Training

def attendance_graph():
    def trainings_to_df(trainings):
        data = dict(
            date = [tr.date for tr in trainings],
            attended = [len(tr.attendees) for tr in trainings],
            advised = [len(tr.advisories) for tr in trainings],
            missing = [len(tr.nonadvisories) for tr in trainings],
            attendance_percent = [len(tr.attendees)/(len(tr.nonadvisories) + len(tr.advisories) + len(tr.attendees))*100  for tr in trainings]
        )
        return pd.DataFrame.from_dict(data).sort_values(['date'])
    
    p = figure(plot_width=800, plot_height=250, x_axis_type="datetime")
    p.title.text = 'Number of pilots attending. Click on legend entries to hide or show data'

    
    data_dict = dict(
        wt_tz1 = trainings_to_df(Training.query.filter_by(sim='wt', geozone='TZ1').all()),
        wt_tz2 = trainings_to_df(Training.query.filter_by(sim='wt', geozone='TZ2').all()),
        il2_tz1 = trainings_to_df(Training.query.filter_by(sim='il2', geozone='TZ1').all()),
        il2_tz2 = trainings_to_df(Training.query.filter_by(sim='il2', geozone='TZ2').all()),
        dcs_tz1 = trainings_to_df(Training.query.filter_by(sim='dcs', geozone='TZ1').all()),
        bms_tz1 = trainings_to_df(Training.query.filter_by(sim='bms', geozone='TZ1').all()),
        bms_tz2 = trainings_to_df(Training.query.filter_by(sim='bms', geozone='TZ2').all()),
    )
    from bokeh.palettes import Colorblind5 as palette
    for key, df, color in zip(data_dict.keys(), data_dict.values(), palette):     
        #p.line(df['date'], df['attendance_percent'], line_width=2, color=color, alpha=0.8, legend=key)
        # change to absolute number as more useful
        p.line(df['date'], df['attended'], line_width=2, color=color, alpha=0.8, legend=key)
    p.legend.location = "top_left"
    p.legend.click_policy="hide"
    return components(p)



def join_date_graph(raw_data, date_format='%m/%d/%Y-%H:%M:%S', date_key='joined_at'):
    try:
        series = [datetime.strptime(m[date_key], date_format).strftime('%Y-%m-%d') for m in raw_data]
    except TypeError:
        series = [m[date_key] for m in raw_data]
    data = [dict(
        type= 'histogram',
	    cumulative=dict(enabled=True),
        x = series)]

    graphJSON = json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)
    return graphJSON

def discord_makeup(raw_data):
    data_dict = {
        'X51 Members': raw_data['n_x51'],
        'Guests & Friends': raw_data['n_guests']
    }
    x = Counter(data_dict)
    data = pd.DataFrame.from_dict(dict(x), orient='index').reset_index().rename(index=str, columns={0:'value', 'index':'sim'})
    data['angle'] = data['value']/sum(x.values()) * 2*pi
    data['color'] = Category20c[3][:2]
    p = figure(plot_width= 500, plot_height=500, title="Discord Server Makeup", toolbar_location=None,
           tools="hover", tooltips="@sim: @value")
    p.wedge(x=0, y=1, radius=0.4,
        start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
        line_color="white", fill_color='color', legend='sim', source=data)
    p.axis.axis_label=None
    p.axis.visible=False
    p.grid.grid_line_color = None

    return components(p)
    """    colorscale = [
            [0,"rgb(80, 80, 80)"],
            [0,"rgb(80, 80, 80)"],

            [0,"rgb(55, 0, 255)"],
            [0.2,"rgb(55, 0, 255)"],
            
            [0.2,"rgb(55, 0, 215)"],
            [0.4,"rgb(55, 0, 215)"],
            
            [0.4,"rgb(55, 0, 175)"],
            [0.4,"rgb(55, 0, 175)"],

            [0.6,"rgb(55, 0, 135)"],
            [0.8,"rgb(55, 0, 135)"],
            
            [0.8,"rgb(55, 0, 95)"],
            [1.0,"rgb(55, 0, 95)"],
           ],
           
            colorscale= [
               [0.0, 'rgb(165,0,38)'], 
               [0.1111111111111111, 'rgb(215,48,39)'], 
               [0.2222222222222222, 'rgb(244,109,67)'], 
               [0.3333333333333333, 'rgb(253,174,97)'], 
               [0.4444444444444444, 'rgb(254,224,144)'], 
               [0.5555555555555556, 'rgb(224,243,248)'], 
               [0.6666666666666666, 'rgb(171,217,233)'], 
               [0.7777777777777778, 'rgb(116,173,209)'], 
               [0.8888888888888888, 'rgb(69,117,180)'], 
               [1.0, 'rgb(49,54,149)']],"""
def world_map():
    members = Member.query.filter_by(active=True).all()
    locations = Counter([countries.lookup(m.location).alpha_3 for m in members if m.location])
    for c in countries:
        if c.alpha_3 not in locations.keys():
            locations[c.alpha_3] = 0
    app.logger.info(locations)
    df = pd.DataFrame(list(locations.items()), columns=['Country', 'Count']) 

    data = [dict(
        type = 'choropleth',
        locations = df['Country'],
        z = df['Count'],
        colorscale = 'Blues',
#        autocolorscale = True,
       reversescale = True,
        marker = dict(
            line = dict (
                color = 'rgb(180,180,180)',
                width = 0.5
            ) ),
        colorbar = dict(
            autotick = False,
            title = 'Number of members'),
      ) ]
    
    layout = dict(
        title = 'The X51 Empire',
        geo = dict(
        showframe = True,
        showland = True,
        showcoastlines = True,
        showcountries = True,
        landcolor = "rgb(229, 229, 229)",
        countrycolor = "rgb(255, 255, 255)" ,
        coastlinecolor = "rgb(255, 255, 255)",
        projection = dict(
            type = 'meractor')
        )
    )
    #fig = dict( data=data, layout=layout )
    #py.iplot( fig, validate=False, filename='x51-world-map' )
    graphJSON = json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)
    return graphJSON
    
    
def join_rank_scatter():
    ranks = PilotRoles.ranks
    rank_int = range(len(ranks))

    members = Member.query.filter_by(active=True).all()
    user_dict= dict(
        join_date = [u.join_date for u in members],
        rank = [u.pilot_rank for u in members]
    )
    p = figure(title = "Rank vs join date")
    p.xaxis.axis_label = 'Join Date'
    p.yaxis.axis_label = 'Rank'
    p.circle(user_dict['join_date'], user_dict['rank'], fill_alpha=0.2, size=10)
    return p

def players_per_platform():
    data = {
        'wt': len(Member.query.filter_by(wt=True, active=True).all()),
        'dcs': len(Member.query.filter_by(dcs=True, active=True).all()), 
        'il2': len(Member.query.filter_by(il2=True, active=True).all()),
        'bms': len(Member.query.filter_by(bms=True, active=True).all()),
        'arma': len(Member.query.filter_by(arma=True, active=True).all()), 
    }
    p = figure(x_range=list(data.keys()), y_range=(0, max(data.values())), plot_width= 350, plot_height=350, title="#of users per platform", toolbar_location=None, tools="")
    p.vbar(x=list(data.keys()), top=list(data.values()), width=0.9 )
    
    return components(p)

def rank_chart():
    data = {rank: len(Member.query.filter_by(pilot_rank=rank, active=True).all()) for rank in PilotRoles.ranks}
    p = figure(x_range=list(data.keys()), y_range=(0, max(data.values())), plot_width= 350, plot_height=350, title="Rank Distribution", toolbar_location=None, tools="")
    p.vbar(x=list(data.keys()), top=list(data.values()), width=0.9 )
    
    return components(p)

def main_sim_pie():
    data_dict = {
        'wt': len(Member.query.filter_by(req_training='wt', active=True).all()),
        'dcs': len(Member.query.filter_by(req_training='dcs', active=True).all()), 
        'il2': len(Member.query.filter_by(req_training='il2', active=True).all()),
        'bms': len(Member.query.filter_by(req_training='bms', active=True).all()),
    }
    x = Counter(data_dict)
    data = pd.DataFrame.from_dict(dict(x), orient='index').reset_index().rename(index=str, columns={0:'value', 'index':'sim'})
    data['angle'] = data['value']/sum(x.values()) * 2*pi
    data['color'] = Category20c[len(x)]
    p = figure(plot_width= 350, plot_height=350, title="Main Platform of Members", toolbar_location=None,
           tools="hover", tooltips="@sim: @value")
    p.wedge(x=0, y=1, radius=0.4,
        start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
        line_color="white", fill_color='color', legend='sim', source=data)
    p.axis.axis_label=None
    p.axis.visible=False
    p.grid.grid_line_color = None

    return components(p)

def rank_pie():
    data_dict = {rank: len(Member.query.filter_by(pilot_rank=rank, active=True).all()) for rank in PilotRoles.ranks}

    x = Counter(data_dict)
    data = pd.DataFrame.from_dict(dict(x), orient='index').reset_index().rename(index=str, columns={0:'value', 'index':'rank'})
    data['angle'] = data['value']/sum(x.values()) * 2*pi
    data['color'] = Set3[len(x)]
    p = figure(plot_width= 350, plot_height=350, title="Ranks of Members. (Hover over segments to see rank)", toolbar_location=None,
           tools="hover", tooltips="@rank: @value")
    p.wedge(x=0, y=1, radius=0.4,
        start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
        line_color="white", fill_color='color', source=data)
    p.axis.axis_label=None
    p.axis.visible=False
    p.grid.grid_line_color = None

    return components(p)

def member_training_graph(member):
    data = {
        'attended': len(member.attended),
        'advised': len(member.advised),
        'missing': len(member.missing),
    }
    p = figure(x_range=list(data.keys()), y_range=(0, max(data.values())), plot_width= 350, plot_height=350, title="Training attendance", toolbar_location=None, tools="")
    p.vbar(x=list(data.keys()), top=list(data.values()), width=0.9 )
    return components(p)

def training_type_pie(member):
    data_dict = {
        'wt': len([t for t in member.attended if t.sim=='wt']),
        'il2': len([t for t in member.attended if t.sim=='il2']),
        'dcs': len([t for t in member.attended if t.sim=='dcs']),
        'bms': len([t for t in member.attended if t.sim=='bms']),
    }
    x = Counter(data_dict)
    data = pd.DataFrame.from_dict(dict(x), orient='index').reset_index().rename(index=str, columns={0:'value', 'index':'sim'})
    data['angle'] = data['value']/sum(x.values()) * 2*pi
    data['color'] = Category20c[len(x)]
    p = figure(plot_width= 350, plot_height=350, title="Training types attended by pilot", toolbar_location=None,
           tools="hover", tooltips="@sim: @value")
    p.wedge(x=0, y=1, radius=0.4,
        start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
        line_color="white", fill_color='color', legend='sim', source=data)
    p.axis.axis_label=None
    p.axis.visible=False
    p.grid.grid_line_color = None

    return components(p)
