import sys
from werkzeug.security import generate_password_hash
from x51 import create_app, db
#from x51.database import db
from x51.models import User
app = create_app()
app.app_context().push()

def new_user(username, email, password, tier=1):
    with app.app_context():
        user = User(username = username, email=email, password = generate_password_hash(password))
        db.session.add(user)
        db.session.commit()

