# FileApi
The FileApi is a RESTful API layer used to interact with the S3 storage.
![](https://i.imgur.com/TcoTwXm.jpg)
## Authentication
Authentication is achieved using a Basic Authentication endpoint `fileapi/login` and the credentials of a valid account for the website.
Regular users will be able to read the file structure of the storage, while those with the "officer" role can request a presigned upload url to put files on the storage.

## Endpoints

### fileapi/login
payload parameters: none

header parameters:
- `"Authorization": "Basic <base64 encoded string of username:password>"`

returns: json with key `token`. This token is valid for 30 minutes and is used to authenticate requests to the other endpoints.

### fileapi/get-file-list
payload parameters: none

header parameters:
- `"Content-Type": "application/json" `
- `"x-access-tokens": <access-token-from-login>`

returns: a json with key `files` containing an array of the absolute path of all files on the storage.

### fileapi/get-upload-token
payload parameters:
- `"object": < absolute upload path for the file (e.g il2/skins/plane/skin.dds) >` 

header parameters:
- `"Content-Type": "application/json" `
- `"x-access-tokens": <access-token-from-login>`

returns: a presigned url that can be used to upload the file directly to the storage. For presigned url usage see: https://levelup.gitconnected.com/use-presigned-url-to-upload-files-into-aws-s3-db6b7a8c2cc9

### fileapi/delete-file
payload parameters:
- `"object": < absolute upload path for the file (e.g il2/skins/plane/skin.dds) >` 

header parameters:
- `"Content-Type": "application/json" `
- `"x-access-tokens": <access-token-from-login>`

returns: success if S3 commands were executed succesfully. Fails silently if the file to delete is not found, therefore user must verify the file is deleted by calling `fileapi/get-file-list` again
