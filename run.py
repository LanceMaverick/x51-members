from x51.logconf import configure_logging
from x51 import create_app
configure_logging()
app = create_app()

if __name__ == "__main__":
    app.run()
