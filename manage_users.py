#import sys
from werkzeug.security import generate_password_hash
from datetime import datetime
import argparse
from x51 import create_app, db, models
#from x51.database import db
#from x51.models import User
app = create_app()
app.app_context().push()
from flask_security import Security, SQLAlchemyUserDatastore, utils
user_datastore = SQLAlchemyUserDatastore(db, models.User, models.Role)
#security = Security(app, user_datastore)

def make_roles():
    user_datastore.find_or_create_role(name='admin', description='Administrator')
    user_datastore.find_or_create_role(name='officer', description='Officer role')
    user_datastore.find_or_create_role(name='instructor', description='Instructor role')
    user_datastore.find_or_create_role(name='curator', description='Mod and skin pack curator role')
    user_datastore.find_or_create_role(name='member', description='Regular X51 member')
    db.session.commit()


def new_user(username, password, roles=[]):
    #encrypted_password = utils.encrypt_password('password')
    #encrypted_password = generate_password_hash(password)
    if not user_datastore.get_user(username):
        user = user_datastore.create_user(username=username, password=utils.hash_password(password))

        for r in roles:
            user_datastore.add_role_to_user(user, r)
        db.session.commit()
        print('User {} created'.format(username))
    else:
        print("User {} already exists.".format(username))

def delete_user(username):
    user = user_datastore.get_user(username)
    if user:
        user_datastore.delete_user(user)
        print('User {} deleted.'.format(username))
        db.session.commit()
    else:
        print("User {} does not exist.".format(username))

def update_password(username, password):
    user = user_datastore.get_user(username)
    if not user:
        print("User {} does not exist.".format(username))
    else:
        user.set_password(password)
        db.session.commit()


def add_role_user(username, role):
    user = user_datastore.get_user(username)
    if not user:
        print("User {} does not exist.".format(username))
    else:
        user_datastore.add_role_to_user(user, role)
        db.session.commit()

def remove_role_user(username, role):
    user = user_datastore.get_user(username)
    if not user:
        print("User {} does not exist.".format(username))
    else:
        user_datastore.add_role_to_user(user, role)
        db.session.commit()

def new_user_ui():
    username = input('USERNAME: ')
    password = input('PASSWORD: ')
    if username and password:
        new_user(username, password)
    else:
        print('username or password is empty')

def delete_user_ui():
    username = input('USERNAME: ')
    if not username:
        print('Username is empty')
    else:
        confirmation = input('Are you sure you want to delete {}? (Y/N)'.format(username))
        if confirmation == 'Y':
            delete_user(username)
        else:
            print('{} not deleted'.format(username))

def confirm_user(username, flag):
    user = user_datastore.get_user(username)
    if flag:
        user.confirmed_at = datetime.now()
        print('User "{}" confirmed at {}'.format(username, datetime.now().strftime('%m/%d/%Y %H:%M')))
    else:
        user.confirmed_at = None
    db.session.commit()
        
def update_password_ui():
    username = input('USERNAME: ')
    password = input('PASSWORD: ')
    confirm = input('CONFIRM PASSWORD: ')
    if password == confirm:
        update_password(username, password)
    else:
        print('Passwords do not match')

def add_role_ui():
    username = input('USERNAME: ')
    role = input('ROLE: ')
    if username and role:
        add_role_user(username, role)
    else:
        print('username or role empty')

def remove_role_ui():
    username = input('USERNAME: ')
    role = input('ROLE: ')
    if username and role:
        remove_role_user(username, role)
    else:
        print('username or role empty')

def confirm_user_ui():
    username = input('USERNAME: ')
    if username:
        confirm_user(username, True)

def unconfirm_user_ui():
    username = input('USERNAME: ')
    if username:
        confirm_user(username, False)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Manage user accounts')
    functions = {'create' : new_user_ui,
             'delete' : delete_user_ui,
             'pass':    update_password_ui,
             'addrole' : add_role_ui,
             'removerole': remove_role_ui,
             'confirm': confirm_user_ui,
             'unconfirm': unconfirm_user_ui
             }
    parser.add_argument('command', choices=functions.keys())
    args = parser.parse_args()
    func = functions[args.command]
    func()

